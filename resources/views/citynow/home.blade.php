@extends('citynow.master')
@section('content')
<!-- slider home page website -->
<section id="slider" class="wow fadeInUp">
    <div class="banner">
    <div class="container">
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>    
                        <div class="banner_text">
                            <h3 class="wow fadeInUp">Citynow</h3>
                            <div class="w3ls-line"> </div>
                            <p>Create tomorrow</p>
                            <div class="w3-button">
                                <a href="{{route('about')}}" class="btn btn-1 btn-1b">Xem thêm</a>
                            </div>
                        </div>
                    </li>
                    <li>    
                        <div class="banner_text">
                            <h3 class="wow fadeInUp">Tham gia với chúng tôi</h3>
                            <div class="w3ls-line"> </div>
                            <p>Ngay và luôn</p>
                            <div class="w3-button">
                                <a href="{{route('career')}}" class="btn btn-1 btn-1b">Xem thêm</a>
                            </div>
                        </div>
                    </li>
                    <li>    
                        <div class="banner_text">
                            <h3 class="wow fadeInUp">Sản phẩm và dịch vụ</h3>
                            <div class="w3ls-line"> </div>
                            <p>Uy tín</p>
                            <div class="w3-button">
                                <a href="{{route('service')}}" class="btn btn-1 btn-1b">Xem thêm</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div>
</section>
<!-- end slider home page website -->

<!-- why us -->
<section id="about-citynow" class="section-lg wow fadeInUp">
    <div class="container">
        <!-- <div class="row"> -->
            <h1 class="text-center">{{ $language_id === 1 ? "Chúng tôi làm gì?" : "" }}</h1>
            @foreach($services as $intro)
                    <div class="col-md-4">
                        <div class="row-title-item">
                            <div class="avatar-title">
                                <img src="{{$intro->image_url or null}}">
                            </div>
                            <h3 class="title text-center">
                                {{$intro->title or null}}
                            </h3>
                            <div class="content">
                                {!! str_limit(strip_tags($intro->introduction), $limit = 300, $end = '...') !!}
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
        <!-- </div> -->
    </div>
</section>
<!-- end why us -->

<!-- client-say -->
<section id="client-say" class="section-lg wow fadeInUp">
    <div class="container">
        <!-- <div class="row"> -->
            <h1 class="text-center">{{ $language_id === 1 ? "Khách hàng nói gì?" : "" }}</h1>
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client-say-item">
                    @foreach($customers as $customer)
                        <div class="item center clients-say-container">
                            <h3 class="clients-say-title">{{$customer->name or null}}</h3>
                            <p class="clients-say-content">{!! str_limit(strip_tags($customer->introduction), $limit = 300, $end = '...') !!}</p>
                        </div>
                    @endforeach
              </div>
            </div>
        <!-- </div> -->
    </div>
    <!-- client -->
    <section id="client" class="wow fadeInUp">
        <div class="container">
            <!-- <div class="row"> -->
                <!-- <h1 class="text-center">{{ $language_id === 1 ? "Khách hàng tiêu biểu" : "" }}</h1> -->
                <div class="col-lg-12">
                    <div class="owl-carousel owl-theme client-item">
                        @foreach($customers as $customer)
                        <div class="logo-image item">
                            <a href="{{$customer->link or null}}">
                                <img src="{{$customer->image_url or null}}" alt="" >
                            </a>
                        </div>
                        @endforeach
                  </div>
                </div>
            <!-- </div> -->     
            <div class="clearfix"></div>
        </div>
    </section>
<!-- client -->
</section>
<!-- client-say -->

<!-- work-at -->
<section id="work-at" class="wow fadeInUp" style="background: url({{$introductions->image_url or null}})">
    <div class="work-at center">
        <div class="container">
            <div class="col-md-6 work-at-left text-center">
                <h2>{{$introductions->title or null}}</h2>
            </div>
            <div class="col-md-6 work-at-right">
                <span class="work-at-content">{!!$introductions->content_text or null!!}</span>
                <span class="work-at-click">
                    <a href="/career">Tham gia ngay!</a>
                </span>
            </div>
        </div>
    </div>
</section>
<!-- work-at -->
<!-- news -->
<section id="news" class="section-lg wow fadeInUp">
    <div class="container">
        <h1 class="text-center">{{ $language_id === 1 ? "Tin tức" : "" }}</h1>
        @foreach($news as $n)
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="news-item">
                <a href="{{route('news-detail', $n->id)}}"><img src="{{$n->image_url or null}}" alt="" ></a>
                <div class="caption">
                    <div class="caption-header">
                        <ul>
                            <li><span>{{$n->create_user_id or null}}</span></li>
                        </ul>
                    </div>
                    <div class="caption-body">
                        <h4>
                            <a href="{{route('news-detail', $n->id)}}">
                                @if ($language_id === 1)
                                    {!! \Illuminate\Support\Str::words(strip_tags($n->title), 10,'...')  !!}
                                @else
                                    {!! str_limit(strip_tags($n->title), $limit = 40, $end = '...') !!}
                                @endif
                            </a>
                        </h4>
                    </div>
                    <div class="caption-footer">
                        <ul>
                            <li><i class="fa fa-clock-o fa-sm"></i><span>{{ $n->created_at or null}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
<!-- end news -->
@endsection
@section('script')
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
            $('body').removeClass('loading');
            }
        });
    });
    $(document).ready(function() {
      var owl = $('.client-item');
      owl.owlCarousel({
        margin: 20,
        nav: true,
        loop: true,
        autoplay: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      })
    })
    jQuery(document).ready(function($) {
      $('.client-say-item').owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 1,
        margin: 30,
        stagePadding: 30,
        smartSpeed: 450,
        autoplay: true
      });
    });
@endsection