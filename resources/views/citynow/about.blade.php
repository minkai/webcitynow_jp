@extends('citynow.master')
@section('content')
    <section class="wow fadeInUp" id="bg-about-us" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
        <div class="bg_header">
            <div class="center container" >
                <div class="title" data-wow-delay="0.2s">
                    {{$header->title or null}}
                </div>
                <div class="content" data-wow-delay="0.5s">
                    {!! $header->content_text or null !!}
                </div>
            </div>
        </div>
    </section>
    <section id="company-infomations" class="section-lg wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="company-informations-img center col-md-4 col-sm-6 col-xs-12">
                    <img src="{{$company_informations->image_url}}" alt="" />
                </div>
                <div class="company-informations-content col-md-8 col-sm-6 col-xs-12">
                    <h3 class="title text-center">
                    {{$company_informations->title or null}}
                    </h3>
                    <div class="content">
                        {!! $company_informations->content_text or null !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="what-we-have" class="section-lg wow fadeInUp">
        <div class="container">
            <h1 class="text-center">{{ $language_id === 1 ? "Những gì chúng tôi có" : "" }}</h1>
            <div class="row">
                @foreach($abouts as $about)
                    <div class="row-title-item wow fadeInUp col-md-4 col-sm-6 col-xs-12">
                        <div class="avatar-title">
                            <img src="{{$about->image_url}}">
                        </div>
                        <h3 class="title text-center">
                            {{$about->title or null}}
                        </h3>
                        <div class="content">
                            {!! str_limit(strip_tags($about->content_text), $limit = 300, $end = '...') !!}
                        </div>
                    </div>
                @endforeach
            </div>

    </div>
    </section>
    <section id="core-value" class="section-lg wow fadeInUp">
        <div class="container">
            <h1 class="text-center">{{ $language_id === 1 ? "Giá trị cốt lõi" : "" }}</h1>
            @foreach($values as $value)
            <div class="col-md-4 col-sm-6 col-xs-12 mt20">
                <div class="row-title-item">
                    <div class="avatar-title">
                        <img src="{{$value->image_url}}">
                    </div>
                    <h3 class="title text-center">
                        {{$value->title or null}}
                    </h3>
                    <div class="content">
                        {!! str_limit(strip_tags($value->content_text), $limit = 300, $end = '...') !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </section>
    <section id="company-img" class="section-lg wow fadeInUp">
        <div class="container-fluid">
            <h1 class="text-center">{{ $language_id === 1 ? "Góc ảnh" : "" }}</h1>
            <div class="row">
                <div class="col-lg-12">
                    <div class="camera_wrap camera_emboss" id="slider-company-img">
                        @foreach($company_imgs as $company_img)
                            <div data-thumb="{{$company_img->image_url}}" data-src="{{$company_img->image_url}}" data-time="1500" data-trasPeriod="4000" data-link="#" data-target="_blank">
                                <div class="camera_caption fadeFromBottom">
                                    {{$company_img->title or null}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    jQuery(function(){
            jQuery('#slider-company-img').camera({
                height: '40%',
                pagination: false,
                thumbnails: false,
            });
 
        });
    var $columns = $('#what-we-have .row-title-item');
    var height = 0;
    $columns.each(function () {
        if($(this).height() > height) {
            height = $(this).height();
        }
    });
    $columns.height(height);
    var $rows = $('#what-we-have .row-title-item');
    $rows.each(function () {
        $(this).find('#what-we-have .row-title-item').height($(this).height());
    });
@endsection