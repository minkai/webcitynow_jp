@extends('citynow.master')
@section('content')
<section id="contact" class="wow fadeInUp" style="background-image: url({{$header->image_url or null }}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
	<div class="bg_header">
		<div class="container center" >
	        <div class="title wow fadeInUp" data-wow-delay="0.2s">
	            {{$header->title or null}}
	        </div>
	        <div class="content wow fadeInUp" data-wow-delay="0.5s">
	            {!! $header->content_text or null !!}
	        </div>
	    </div>
	</div>
</section>
<section id="contavt-vi" class="wow fadeInUp" style="padding: 50px 0 25px 0;">
	<div class="container">
		<h3>Công ty TNHH Citynow</h3>
		<p>298 Ba Tháng Hai, Phường 12, Quận 10, Thành phố Hồ Chí Minh, Việt Nam.</p>
		<p>Số điện thoại: +84 28 3865 8926</p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.53416276843!2d106.66937981520617!3d10.770339692325622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752edd93bdb1d9%3A0x3fd03cda29c8a82e!2sCityNow+Co.Ltd!5e0!3m2!1svi!2s!4v1537244499403" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</section>
<section id="contact-jp" class="wow fadeInUp" style="padding: 25px 0 50px 0;">
	<div class="container">
		<h3>Citynow Asia Inc.</h3>
		<p>Tầng 7 – Tòa nhà Iino, 3-1-3 Hatchobori, Chuo-ku, Tokyo, Japan, 104-0032</p>
		<p>Tel: +81 80 3755 6327</p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.029998334426!2d139.7719851154192!3d35.676263337851374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188958c75adc0d%3A0xca56cfb41e2475f3!2zSmFwYW4sIOOAkjEwNC0wMDMyIFTFjWt5xY0tdG8sIENoxavFjS1rdSwgSGF0Y2jFjWJvcmksIDMgQ2hvbWXiiJIx4oiSMyDpo6_ph47jg5Pjg6s36ZqO!5e0!3m2!1sen!2s!4v1537244610921" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</section>
@endsection
