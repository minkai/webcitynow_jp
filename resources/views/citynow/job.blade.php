@extends('citynow.master')
@section('content')
    <div id="job">
        <div class="center bg-job wow fadeInUp" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
            <div class="bg_header">
                <div class=" container center" >
                <div class="title wow fadeInUp" data-wow-delay="0.2s">
                    {{$recuitment->title or null}}
                </div>
                <div class="content wow fadeInUp" data-wow-delay="0.5s">
                    {!! $recuitment->location or null !!}
                </div>
            </div>
        </div>
        </div>
        <div class="job ">
            <div class="container">
                
            <div class="col-md-7 col-sm-7 col-sx-12 job-detail wow fadeInUp">
                <h1 class="title"><?php echo ($language_id === 1 ? 'Mô tả công việc' : '仕事の説明:') ?></h1>
                <div class="content">
                    {!! $recuitment->job_description or null !!}
                </div>
                <h1 class="title"><?php echo ($language_id === 1 ? 'Yêu cầu công việc' : '仕事の要求:') ?></h1>
                <div class="content">
                    {!! $recuitment->job_requirement or null !!}
                </div>
            </div>
            <div class="col-md-5 col-sm-5 col-sx-12 wow fadeInUp">
                <form action="{{route('apply-online')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row form-apply">
                        <div class="row"></div>
                        @if(Session::has('flag'))
                            <div class="alert alert-{{Session::get('flag')}}">{{Session::get('message')}}</div>
                        @endif
                        <div class="row">
                            <h1 class="title"><?php echo ($language_id === 1 ? 'Ứng tuyển online' : 'オンラインで申し込む') ?></h1>
                            <div class="form-group">
                                <?php echo ($language_id === 1 ? 'Đây là các thông tin bắt buộc phải nhập' : 'これは必須情報です') ?>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="<?php echo ($language_id === 1 ? 'Hãy nhập email' : 'メールを入力してください') ?>" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="<?php echo ($language_id === 1 ? 'Hãy nhập họ và tên' : '名前を入力してください') ?>" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group input-group input-file" name="file_cv">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" type="button"><?php echo ($language_id === 1 ? 'Upload' : 'アップロード') ?></button>
                                </span>
                                    <input type="text" class="form-control" placeholder='<?php echo ($language_id === 1 ? 'Upload file CV: .doc, .docx, .pdf' : 'CVファイルをアップロードする：.doc、.docx、.pdf') ?>'/>
                                    <span class="input-group-btn">
                                     <button class="btn btn-warning btn-reset" type="button"><?php echo ($language_id === 1 ? 'Reset' : 'リセット') ?></button>
                                </span>
                            </div>
                            <div class="form-group">
                                <textarea rows="10" class="form-control" name="introduction" placeholder="<?php echo ($language_id === 1 ? 'Những kỹ năng, dự án làm việc hoặc thành tích nào làm bạn trở thành một ứng cử viên mạnh?' : 'どんなスキル、プロジェクト、業績があなたを強力な候補にしていますか？') ?>"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" style="min-width: 120px"><?php echo ($language_id === 1 ? 'Ứng tuyển' : 'アプリケーション') ?></button>
                            </div>
                            @if(count($errors)>0)
                                <div class="alert alert-danger pt10">
                                    @foreach($errors->all() as $err)
                                        {{$err}}
                                    @endforeach
                                </div>
                            @endif
                            @if(Session::has('success_message'))
                                <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                            @endif
                            @if(Session::has('success_fail'))
                                <div class="alert alert-danger pt10">{{Session::get('success_fail')}}</div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    bs_required_input_file();
@endsection