@extends('citynow.master')
@section('content')
<section id="bg-career" class="wow fadeInUp" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
    <div class="bg_header">
        <div class=" container center" >
            <div class="title wow fadeInUp" data-wow-delay="0.2s">
                {{$header->title or null}}
            </div>
            <div class="content wow fadeInUp" data-wow-delay="0.5s">
                {!! $header->content_text or null !!}
            </div>
        </div>
    </div>
</section>

@if (!$recuitments->isEmpty())
    <section id="job-opening" class="section-lg wow fadeInUp">
        <div class="container">
            <h1 class="text-center">
                {{ $language_id === 1 ? "Tuyển dụng" : "" }}
            </h1>
            <div class="list-job">
                @foreach($recuitments as $recuit)
                    <div class="job-item row">
                        <div class="col-md-5 col-sm-12 col-xs-12 col-item">
                            <div class="title"><a href="{{route('job', $recuit->id)}}">{{$recuit->title or null}}</a></div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 col-item">
                            <div class="time">{{$recuit->type or null}}</div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-item">
                            <div class="location">{{$recuit->location or null}}</div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 col-item">
                            <div class="btn-apply">
                                <a href="{{route('job', $recuit->id)}}" class="btn btn-primary">{{ $language_id === 1 ? "Apply" : "応募" }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
<section id="welfare" class="section-lg wow fadeInUp">
    <div class="container">
        <h1 class="text-center">
                {{ $language_id === 1 ? "Phúc lợi công ty" : "" }}
        </h1>
        <div class="welfare-item-list row">
            <div class="row-title">
                @foreach($welfares as $welfare)
                    <div class="welfare-item wow fadeInUp col-md-4 col-sm-6 col-xs-12 wow fadeInUp">
                        <div class="welfare-img">
                           <div class="image" style="background: url({{$welfare->image_url or null}}) no-repeat center; width: 100%;" alt=""></div>
                        </div>
                        <div class="welfare-content">
                            <div class="title">
                                {{$welfare->name or null}}
                            </div>
                            <div class="content">
                                {!! $welfare->description or null !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<section id="team-testimonial" class="section-lg wow fadeInUp">
        <div class="container">
            <h1 class="text-center">
                {{ $language_id === 1 ? "Nhân sự tiêu biểu" : "" }}
            </h1>
        @foreach($teamTestimonials as $index => $teamTestimonial)
        <div class="team-testimonial-item wow fadeInUp" data-wow-delay="0.3s">
            <div class="team-testimonial-image center col-md-4 col-xs-12 {{$index%2!==0 ? 'right' : 'left'}}">
                <img class="image" src="{{$teamTestimonial->image_url}}" alt="" width="100%" />
            </div>

            <div class="team-testimonial-content col-md-8 col-xs-12 {{$index%2!==0 ? 'left' : 'right'}}">
                <div class="title {{$index%2!==0 ? 'text-right' : 'text-left'}}">
                    {{$teamTestimonial->name or null}}
                </div>
                <div class="position {{$index%2!==0 ? 'text-right' : 'text-left'}}">
                    {{$teamTestimonial->position or null}}
                </div>
                <div class="content">
                    {!! $teamTestimonial->description or null !!}
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        @endforeach
    </div>
</section>

@endsection