@extends('citynow.master')
@section('content')
    <div id="clients">
        <div class="center bg-client" style="background-image: url({{$header->image_url}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto;">
            <div class=" container center" >
            <div class="title">
                {{$header->title or null}}
            </div>
            <div class="content">
                {!! $header->content_text or null !!}
            </div>
        </div>
        </div>
        <div class="clients">
            <div class="title-value center">
                <img src="citynow/images/page/symbol-xanh.png">
                <span class="client-title">
                    {{ $language_id === 1 ? "Khách hàng tiêu biểu" : "典型的な顧客" }}
                    <div class="separate-title">
                        <div class="content"></div>
                    </div>
                </span>
            </div>
            <div class="list-client">
                @foreach($customers as $customer)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="row client-item">
                            <div class="image-client">
                                <a href="{{$customer->link}}">
                                    <img src="{{$customer->image_url}}" alt="" style="width: 100%;">
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection