<section id="footer">
    <div style="max-width: 1600px; margin:  auto;">
        <div class="footer">
            <div class="row footer-contact">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="title-footer contact-line">{{$footer->title_vn or null}}</div>
                    <div class="contact-line">
                        <i class="fa fa-map-marker fa-2x"></i>{{$footer->address_vn or null}}
                    </div>
                    <div class="contact-line">
                        <i class="fa fa-phone fa-2x"></i>{{$footer->phone_vn or null}}
                    </div>
                    <div class="contact-line">
                        <i class="fa fa-envelope fa-2x"></i>{{$footer->email_vn or null}}
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="title-footer contact-line">{{$footer->title_jp or null}}</div>
                    <div class="contact-line">
                        <i class="fa fa-map-marker fa-2x"></i>{{$footer->address_jp or null}}
                    </div>
                    <div class="contact-line">
                        <i class="fa fa-phone fa-2x"></i>{{$footer->phone_jp or null}}
                    </div>
                    <div class="contact-line">
                        <i class="fa fa-envelope fa-2x"></i>{{$footer->email_jp or null}}
                    </div>
                </div>
            </div>
            <div class="separate-footer">
            </div>
            <div class="copyright">
                <div class="col-md-6 col-sm-6 col-xs-12 copyright-text"> Citynow 2018, All right reserved</div>
                <div class="col-md-6 col-sm-6 col-xs-12 copyright-social">
                    <a href="{{$footer->link_instagram or null}}">
                        <i class="fa fa-instagram fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="{{$footer->link_twitter or null}}">
                        <i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="{{$footer->link_facebook or null}}">
                        <i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>