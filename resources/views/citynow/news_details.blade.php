@extends('citynow.master')
@section('content')
<section class="wow fadeInUp" id="bg-story-detail" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
        <div class="bg_header">
            <div class=" container center" >
                <div class="title wow fadeInUp" data-wow-delay="0.2s">
                    {{$header->title or null}}
                </div>
                <div class="content wow fadeInUp" data-wow-delay="0.5s">
                    {!! $header->content_text or null !!}
                </div>
            </div>
        </div>
</section>
<section id="story-detail">
    <div class="story-detail wow fadeInUp">
        <div class="container">
            <div class="title-value">
                {!! $news->title or null !!}
            </div>
            <div class="location">
                <div>
                    <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                    {{ date('d-m-Y', strtotime($news->created_at)) }}
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="content">
                {!! $news->content or null !!}
            </div>
        </div>
    </div>
    <div class="latest-story wow fadeInUp">
        <h2 class="center">
            {{ $language_id === 1 ? "Tin gần nhất" : "" }}
        </h2>
        <div class="container">
            <div class="row">
                @foreach($latest_news as $n)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="latest-story-item">
                            <div class="latest-story-image" sty>
                                <a href="{{route('news-detail', $n->id)}}"><img src="{{$n->image_url or null}}" alt="" width="100%"></a>
                            </div>
                            <div class="clearfix"></div>
                            <div class="details">
                                <div class="title">
                                    <a href="{{route('news-detail', $n->id)}}">
                                        @if ($language_id === 1)
                                            {!! \Illuminate\Support\Str::words(strip_tags($n->title), 10,'...')  !!}
                                        @else
                                            {!! str_limit(strip_tags($n->title), $limit = 40, $end = '...') !!}
                                        @endif
                                    </a>
                                </div>
                                <div class="create-at">
                                    <i class="fa fa-clock-o fa-sm"></i>{{ $n->created_at or null}}
                                </div>
                                <div class="content">
                                    @if ($language_id === 1)
                                        {!! \Illuminate\Support\Str::words(strip_tags($n->content), 40,'...')  !!}
                                    @else
                                        {!! str_limit(strip_tags($n->content), $limit = 200, $end = '...') !!}
                                    @endif
                                </div>
                                <div class="show-more text-right">
                                    <a href="{{route('news-detail', $n->id)}}">Xem Thêm</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
@endsection