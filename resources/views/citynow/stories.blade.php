@extends('citynow.master')
@section('content') 
<section id="bg-stories" class="wow fadeInUp" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
    <div class="bg_header">
        <div class="container center" data-wow-delay="0.2s">
            <div class="title wow fadeInUp" >
                {{$header->title or null}}
            </div>
            <div class="content wow fadeInUp" data-wow-delay="0.5s">
                {!! $header->content_text or null !!}
            </div>
        </div>
    </div>

</section>
<section id="stories" class="wow fadeInUp">
    <div class="container">
        <h1 class="center">{{ $language_id === 1 ? "Tin tức" : "" }}</h1>
        <div class="list-stories">
            @foreach($news as $n)
            <div class="row news-item wow fadeInUp" data-wow-delay="0.4s">
                <div class="image-stories col-md-4 col-sm-12 col-xs-12">
                    <a href="{{route('news-detail', $n->id)}}">
                        <img src="{{$n->image_url}}" alt="" style="width: 100%;">
                    </a>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="title-news">
                        <a href="{{route('news-detail', $n->id)}}">
                            {{$n->title or null}}
                        </a>
                    </div>
                    <div class="create-at">
                        <i class="fa fa-clock-o fa-sm"></i>{{ $n->created_at or null}}
                    </div>
                    <div class="content">
                        @if ($language_id === 1)
                            {!! \Illuminate\Support\Str::words(strip_tags($n->content), 40,'...')  !!}
                        @else
                            {!! str_limit(strip_tags($n->content), $limit = 500, $end = '...') !!}
                        @endif
                    </div>
                    <div class="right">
                        <a class="show-more" href="{{route('news-detail', $n->id)}}">
                            Xem thêm
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="clearfix">
                <div class="row">
                    <div class="pagination right">{{ $news->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection