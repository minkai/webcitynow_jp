<section class="navbar-info" id="nav">
  <nav class="navbar navbar-default navbar-me">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed menu-collapsed-button" data-toggle="collapse" data-target="#navbar-primary-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand site-logo" href="#">
          <img class="logo-default" src="<?php echo $language_id === 1 ? 'citynow/images/logo/logovn.png' : 'citynow/images/logo/logojp.png' ?>" alt width="149" height="42">
        </a>
      </div>
        
      <div class="collapse navbar-collapse navbar-right  header-right-menu navbar-content" id="navbar-primary-collapse">
        <ul class="nav navbar-nav ">
          @foreach($menus as $index => $m)
          <li class="{{ (\Request::route()->getName() == $m->alias) ? 'active' : '' }}"><a class="{{$m->alias}}" href="{{route($m->alias)}}">{{$m->name or null}}</a></li>
          @endforeach
          <li class="{{ (\Request::route()->getName() == 'citynowsa') ? 'active' : '' }}"><a class="" href="{{url('citynowsa')}}">CitynowSA</a></li>
          <li class="{{ (\Request::route()->getName() == 'contact') ? 'active' : '' }}"><a class="" href="{{url('contact')}}">Liên hệ</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div>
   </nav>
</section>
<!-- end of navbar-->
