@extends('citynow.master')
@section('content')
<section class="wow fadeInUp" id="bg-service" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
    <div class="bg_header">
            <div class=" container center" >
        <div class="title wow fadeInUp" data-wow-delay="0.2s">
            {{$header->title or null}}
        </div>
        <div class="content wow fadeInUp" data-wow-delay="0.5s">
            {!! $header->content_text or null !!}
        </div>
    </div>
    </div>
</section>
<section id="service" class="section-lg wow fadeInUp">
    <div class="service">
        <div class="container">
            <h1 class="text-center">{{ $language_id === 1 ? "Sản phẩm và Dịch vụ" : "" }}</h1>
            <div class="service-tab-container row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 service-tab-menu">
                    <div class="list-group">
                        @foreach($services as $index => $service)
                            <a href="#{{$service->id}}" class="list-group-item text-center {{$index === 0 ? 'active' : ''}}">
                                <span>{{$service->title or null}}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 service-tab wow fadeInUp">
                    @foreach($services as $index => $service)
                        <div class="service-tab-content {{$index == 0 ? 'active' : ''}}">
                            
                            <div class="service-image">
                                <img src="{{$service->image_url}}" alt="">
                            </div>
                            <div class="title center">
                                <h3>{{$service->title or null}}</h3>
                            </div>
                            <div class="content center">
                                {!! $service->introduction !!}
                            </div>
                            <div class="center">
                                <a class="button button-primary" href="{{route('service-detail', $service->id)}}"><?php echo ($language_id === 1 ? 'Xem thêm' : 'もっと見る') ?></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="service-mobile">
                <ul class="serviceSlider">
                    @foreach($services as $index => $service)
                        <li>
                            <div class="service-image">
                                <img src="{{$service->image_url}}" alt="">
                            </div>
                            <div class="title center">
                                <h3>{{$service->title or null}}</h3>
                            </div>
                            <div class="content pl50 pr50 center mt30">
                                {!! $service->introduction !!}
                            </div>
                            <div class="center mt30">
                                <a class="button button-primary" href="{{route('service-detail', $service->id)}}"><?php echo ($language_id === 1 ? 'Xem thêm' : 'もっと見る') ?></a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
</section>
<section id="client" class="wow fadeInUp">
    <div class="container">
            <h1 class="text-center">{{ $language_id === 1 ? "Khách hàng tiêu biểu" : "" }}</h1>
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme client-item-service">
                    @foreach($customers as $customer)
                    <div class="logo-image item">
                        <a href="{{$customer->link or null}}">
                            <img src="{{$customer->image_url or null}}" alt="" >
                        </a>
                    </div>
                    @endforeach
              </div>
            </div>
        <!-- </div> -->     
        <div class="clearfix"></div>
    </div>
</section>

@endsection
@section('script')
    $('.serviceSlider').bxSlider();
    $(document).ready(function() {
        $(".service-tab-content").first().addClass("active");
        $(".list-group-item").first().addClass("active");
    });
    $(document).ready(function() {
      var owl = $('.client-item-service');
      owl.owlCarousel({
        margin: 20,
        nav: true,
        loop: true,
        autoplay: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 5
          }
        }
      })
    })
@endsection