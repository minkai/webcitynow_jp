@extends('citynow.master')
@section('content')
    <div id="career">
        <div class="career-introduction center">
            <div class="custom-container">
                {{--<div class="title">--}}
                    {{--Career--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                    {{--Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopsloi laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunot explicabo. Nemo ernim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sedopk quia consequuntur magni dolores eos qui rationesopl voluptatem sequi nesciunt. Neque porro quisquameo est, qui dolorem ipsum quia dolor sit amet, eopsmiep consectetur, adipisci velit, seisud quia non numquam eius modi tempora incidunt ut labore et dolore wopeir magnam aliquam quaerat voluptatem eoplmuriquisqu--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="custom-container">
            <div class="title-value center">
                Welfare
            </div>
            <div class="row-title">
                @for ($i = 0; $i < 6; $i++)
                    <div class="col-md-4">
                        <div class="row-title-item">
                            <div class="avatar-title">
                                <img src="citynow/images/page/avatar.jpg" alt="" width="100%">
                            </div>
                            <div class="title">
                                Title
                            </div>
                            <div>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore.
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
            <div class="title-value center">
                Team Testimonial
            </div>
            @for($i = 0; $i < 3; $i++)
                <div class="career-item">
                    @if ($i % 2 === 0)
                        <div class="col-md-4">
                            <div class="career-image">
                                <img src="citynow/images/page/no-image.jpg" alt="" width="100%">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="career-content">
                                <div class="title">
                                    Name
                                </div>
                                <div class="content">
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopsloi laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunot explicabo. Nemo ernim
                                    ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sedopk quia consequuntur magni dolores eos qui rationesopl voluptatem sequi nesciunt. Neque porro quisquameo est, qui dolorem ipsum quia dolor sit amet, eopsmiep consectetur, adipisci velit, seisud quia non numquam eius modi tempora incidunt ut labore et dolore wopeir magnam aliquam quaerat voluptatem eoplmuriquisqu
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-8">
                            <div class="career-content">
                                <div class="title">
                                    Name
                                </div>
                                <div class="content">
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopsloi laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunot explicabo. Nemo ernim
                                    ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sedopk quia consequuntur magni dolores eos qui rationesopl voluptatem sequi nesciunt. Neque porro quisquameo est, qui dolorem ipsum quia dolor sit amet, eopsmiep consectetur, adipisci velit, seisud quia non numquam eius modi tempora incidunt ut labore et dolore wopeir magnam aliquam quaerat voluptatem eoplmuriquisqu
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="career-image">
                                <img src="citynow/images/page/no-image.jpg" alt="" width="100%">
                            </div>
                        </div>
                    @endif
                        <div class="clearfix"></div>
                </div>
            @endfor
        </div>
        <div class="job-opening">
            <div class="title center">
                Jobs Opening
            </div>
            <div class="container">
                @for($i = 0; $i < 3; $i++)
                    <div class="job-item">
                        <div class="title">Job Title</div>
                        <div class="content">
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopsloi laudantium, totam rem aperiam
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
@endsection