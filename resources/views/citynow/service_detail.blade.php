@extends('citynow.master')
@section('content')
    <section class="wow fadeInUp" id="bg-service-detail" style="background-image: url({{$header->image_url or null}}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
        <div class="bg_header">
            <div class=" container center" >
                <div class="title wow fadeInUp" data-wow-delay="0.2s">
                    {{$header->title or null}}
                </div>
                <div class="content wow fadeInUp" data-wow-delay="0.5s">
                    {!! $header->content_text or null !!}
                </div>
            </div>
        </div>
    </section>
    <section id="service-detail" class="section-lg wow fadeInUp">
        <div class="container">
            <div class="custom-container">
                <h1 class="text-center wow fadeInUp">{{ $service->title or null }}</h1>
                <div class="image center wow fadeInUp">
                    <img src= "{{$service->image_url or null}}" alt= ""/>
                </div>
                <div class="introduction wow fadeInUp">
                    {!! $service->introduction or null !!}
                </div>
                <div class="content wow fadeInUp">
                    {!! $service->content_text or null !!}
                </div>
            </div>
            <div class="center"><a href="{{route('service')}}" class="button button-primary">Quay lại</a></div>
        </div>
    </section>
@endsection