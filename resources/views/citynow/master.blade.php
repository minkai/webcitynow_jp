<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <title>Citynow</title>
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="citynow/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="citynow/assets/fonts/webfonts/Montserrat.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="citynow/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="citynow/assets/css/colorbox.css">
    <link rel="stylesheet" href="citynow/assets/bxslider/bxslider.css">
    <link rel="stylesheet" href="citynow/assets/css/spacing.css">
    <link rel="stylesheet" href="citynow/assets/css/style.css">
    <link rel="stylesheet" href="citynow/assets/css/about.css">
    <link rel="stylesheet" href="citynow/assets/css/service.css">
    <link rel="stylesheet" href="citynow/assets/css/service_detail.css">
    <link rel="stylesheet" href="citynow/assets/css/story.css">
    <link rel="stylesheet" href="citynow/assets/css/storydetail.css">
    <link rel="stylesheet" href="citynow/assets/css/career.css">
    <link rel="stylesheet" href="citynow/assets/css/job.css">
    <link rel="stylesheet" href="citynow/assets/css/home.css">
    <link rel="stylesheet" href="citynow/assets/css/news.css">
    <link rel="stylesheet" href="citynow/assets/css/contact.css">
    <link rel="stylesheet" href="citynow/assets/css/client.css">
    <link rel="stylesheet" href="citynow/assets/css/login.css">
    <link rel="stylesheet" href="citynow/assets/css/citynowsa.css">
    <link rel="stylesheet" href="citynow/assets/flexslider/css/flexslider.css">
    <link rel="stylesheet" href="citynow/assets/cameraslider/css/camera.css">
    <link rel="stylesheet" href="citynow/assets/owlcarosel/css/docs.theme.min.css">
    <link rel="stylesheet" href="citynow/assets/owlcarosel/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="citynow/assets/owlcarosel/owlcarousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="citynow/assets/owlcarosel/css/animate.css">


</head>
<body class="custom-container">
@include('citynow.header')
<div class="main-content">
    @yield('content')
</div> <!-- .container -->
@include('citynow.footer')
<script src="citynow/assets/js/jquery.min.js"></script>
<script src="citynow/assets/js/bootstrap.min.js"></script>
<script src="citynow/assets/js/jquery.colorbox-min.js"></script>
<script src="citynow/assets/bxslider/bxslider.js"></script>
<script src="citynow/assets/js/vertical-tab.js"></script>
<script src="citynow/assets/js/citynow.js"></script>
<script src="citynow/assets/js/manage.js"></script>

<script src="citynow/assets/cameraslider/js/camera.min.js"></script>
<script src="citynow/assets/cameraslider/js/jquery.easing.1.3.js"></script>
<script src="citynow/assets/cameraslider/js/jquery.mobile.customized.min.js"></script>
<script src="citynow/assets/owlcarosel/owlcarousel/owl.carousel.js"></script>
<script src="citynow/assets/flexslider/js/jquery.flexslider.js"></script>

<script>
    @yield('script')
    $(document).ready(function($) {
        $(window).scroll(function(){
            if($(this).scrollTop()>150){
                $(".header-bottom").addClass('fixNav')
            }else{
                $(".header-bottom").removeClass('fixNav')
            }
        });
        // $('.bxslider').bxSlider();
        $("div.service-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.service-tab>div.service-tab-content").removeClass("active");
            $("div.service-tab").find(".service-tab-content").addClass("hide");
            $("div.service-tab>div.service-tab-content").eq(index).addClass("active");
            $("div.service-tab>div.service-tab-content").eq(index).removeClass("hide");
        });
        var url = window.location.pathname;
        var lstUrl = url.split('/');
        $('.list-menu .menu-navigation li').find('.' + lstUrl[1]).addClass('active');
        $(".alert-success, .alert-danger").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert-success, .alert-danger").slideUp(500);
        });
    });
    $(window).scroll(function() {
        if($(this).scrollTop()>5) {
            $( ".navbar-me" ).addClass("fixed-me");
        } else {
            $( ".navbar-me" ).removeClass("fixed-me");
        }
    });
    jQuery(function(){
            jQuery('#slider_home').camera({
                height: '40%',
                pagination: false,
                thumbnails: false,
            });

        });
    // owlcarosel
</script>
<script src="citynow/assets/js/wow.min.js"></script>
<script type="text/javascript">
    new WOW().init();
</script>
</body>
</html>
