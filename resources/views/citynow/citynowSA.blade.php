@extends('citynow.master')
@section('content')
<section id="keySA" clas="wow fadeInUp" style="background-image: url({{$header->image_url or null }}); background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
    <div class="bg_header">
    	<div class="container center" >
        <div class="title wow fadeInUp" data-wow-delay="0.2s">
            {{$header->title or null}}
        </div>
        <div class="content wow fadeInUp" data-wow-delay="0.5s">
            {!! $header->content_text or null !!}
        </div>
    </div>
    </div>
</section>
<section id="why-choose-us" class="section-lg wow fadeInUp">
	<div class="container">
		<div class="center" style="max-width: 400px; margin: auto; margin-bottom: 60px">
			<img class="center" src="citynow/images/page/citynowsa/logoSA.png" width="100%" alt="">
		</div>
		<div class="why-choose-us-heading text-center">
			<h2>Vì sao chọn chúng tôi?</h2>
			<div class="agileits-line"> </div>
		</div>
		<div class="why-choose-us-content">
			<div class="why-choose-us-item wow fadeInUp col-md-3 col-sm-6 col-xs-12 text-center" data-wow-delay="0.2s">
				<img class="center" src="citynow/images/page/citynowsa/Asset 1.png">
				<h4>Cung cấp thông tin trường học</h4>
				<p>CitynowSA hợp tác và liên kết với rất nhiều trường đại học, cao đẳng ở Nhật Bản. Sẵn sàng giúp đỡ bạn tìm được cho mình ngôi trường phù hợp nhất.</p>
			</div>
			<div class="why-choose-us-item wow fadeInUp col-md-3 col-sm-6 col-xs-12 text-center" data-wow-delay="0.5s">
				<img class="center" src="citynow/images/page/citynowsa/Asset 2.png">
				<h4>Hỗ trợ đến với ngôi trường mơ ước</h4>
				<p>Chúng tôi mong muốn và sẽ tạo mọi điều kiện tốt nhất giúp đỡ các học viên được đến với ngôi trường mà các bạn hằng ao ước.</p>
			</div>
			<div class="why-choose-us-item wow fadeInUp col-md-3 col-sm-6 col-xs-12 text-center" data-wow-delay="0.7s">
				<img class="center" src="citynow/images/page/citynowsa/Asset 3.png">
				<h4>Hỗ trợ công việc bán thời gian</h4>
				<p>Ngoài thời gian học tập tại trường, nếu bạn muốn tìm kiếm một công việc bán thời gian, chúng tôi rất sẵn lòng giới thiệu việc làm tốt nhất cho các bạn.</p>
			</div>
			<div class="why-choose-us-item wow fadeInUp col-md-3 col-sm-6 col-xs-12 text-center" data-wow-delay="0.9s">
				<img class="center" src="citynow/images/page/citynowsa/Asset 4.png">
				<h4>Hỗ trợ tìm kiếm việc làm</h4>
				<p>CitynowSA cam kết giúp các học viên sau khi tốt nghiệp tìm kiếm được công ăn việc làm ổn định, tạo điều kiện phát triển cho học viên nếu có ý định làm việc tại Nhật.</p>
			</div>
		</div>
	</div>
</section>
<section id="gaikokujinnavi" class="wow fadeInUp">
	<div class="container center">
		<div class="center" style="max-width: 400px; margin: auto;">
			<img src="citynow/images/page/citynowsa/gaijin-v2.png" width="100%" alt="">
		</div>
		<div class="w3-button center">
			<a href="https://www.gaikokujinnavi.com/vn" class="btn btn-1 btn-1b">Xem thêm</a>
		</div>
		<div class="introgaikokujinnavi">
			<p>
				GaikokujinNavi là một sản phẩm dịch vụ trực tuyến thuộc công ty Citynow Asia. Đây là cổng thông tin online chuyên cung cấp dịch vụ giới thiệu việc làm cho người biết tiếng Nhật và hỗ trợ thông tin của các trường tiếng Nhật tiếp nhận du học sinh uy tín tại Nhật Bản.
			</p>
		</div>
	</div>
</section>
<section id="informations" class="section-lg wow fadeInUp">
	<div class="container">
		<div class="informations-main">
			<div class="informations-heading">
				<h2>Thông tin</h2>
				<div class="agileits-line"> </div>
			</div>
			<div class="informations-bottom">
				<div class="informations-grid wow fadeInUp">
					<div class="col-md-6 informations-grid-left">
						<div class="informations-icon"><i class="fa fa-fighter-jet" aria-hidden="true"></i></div> 
						<div class="informations-text">
							<h4><a href="https://www.gaikokujinnavi.com/vn/schools" target="_blank">Du học</a></h4>
							<p>Du học Nhật Bản hiện nay đang là một xu hướng, không chỉ riêng với các bạn trẻ, mà còn dành cho tất cả những ai muốn trải nghiệm một môi trường học tập và làm việc mới chuyên nghiệp hơn.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 informations-grid-right">
						<img src="citynow/images/page/citynowsa/newsSa.jpg" alt="" />
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="informations-grid wow fadeInUp">					
					<div class="col-md-6 informations-grid-right ulta-grid">
						<img src="citynow/images/page/citynowsa/japan_culture.jpg" alt="" />
					</div>
					<div class="col-md-6 informations-grid-left ulta-grid">
						<div class="informations-icon"><i class="fa fa-file" aria-hidden="true"></i></div> 
						<div class="informations-text">
							<h4><a href="https://www.gaikokujinnavi.com/vn/jobs" target="_blank">Việc làm</a></h4>
							<p>Những thông tin về việc làm luôn được chúng tôi cập nhật hằng ngày và liên tục. Giúp bạn có được một sự lựa chọn hoàn hảo cho công việc tương lai.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="informations-grid wow fadeInUp">
					<div class="col-md-6 informations-grid-left">
						<div class="informations-icon"><i class="fa fa-cogs" aria-hidden="true"></i></div> 
						<div class="informations-text">
							<h4><a href="https://www.gaikokujinnavi.com/vn/news" target="_blank">Tin tức</a></h4>
							<p>Bản tin Nhật Bản với những thông tin quan trọng trong ngày, những tin tức mới nhất liên quan đều sẽ được cập nhật sớm và đầy đủ nhất.</p>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 informations-grid-right">
						<img src="citynow/images/page/citynowsa/japan_day_logo.jpg" alt="" />
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</section>
<section id="informations-bottom" class="wow fadeInUp">
	<div class="text-grid">
		<div class="container">
			<h4>Hãy để chúng tôi chăm sóc cho tương lai của bạn!</h4>
			<p>Với niềm khao khát và một lòng tin mãnh liệt, đội ngũ Citynow - CitynowSA đều ý thức rõ trọng trách mà mỗi người đang mang trên vai, chúng tôi hy vọng sẽ xoá mờ khoảng cách về địa lý giữa Việt Nam và Nhật Bản, đem văn hoá và con người hai nước xích lại gần nhau hơn. Nhằm tạo nên một cộng đồng phát triển, vững mạnh và hoà hợp.</p>
		</div>
	</div>
</section>
<section id="company-infomations" class="section-lg wow fadeInUp">
	<div class="container">
		<div class="row">
				<div class="company-infomations-heading">
					<h2>Thông tin công ty</h2>
					<div class="agileits-line"> </div>
				</div>
				<div class="company-infomations-content col-md-7 col-sm-12">
					<table class="table table-bordered company-table">
						<thead>
							<tr>
							  <th scope="col" style="width: 30%%">#</th>
							  <th scope="col">Thông tin công ty</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							  <th>Thông tin công ty</th>
							  <td>Công ty TNHH Du Học Citynow</td>
							</tr>
							<tr>
							  <th>Địa chỉ</th>
							  <td>298 Lê Hồng Phong, Phường 12, Quận 10, Tp. HCM, Việt Nam</td>
							</tr>
							<tr>
							  <th>Vốn điều lệ</th>
							  <td></td>
							</tr>
							<tr>
							  <th>Năm thành lập</th>
							  <td>2018</td>
							</tr>
							<tr>
							  <th>Giám đốc đại diện</th>
							  <td>Vũ Lan Hương</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-5 col-sm-12 company-infomations-img">
					<img src="citynow/images/page/citynowsa/citysa.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<section id="clientSA" class="wow fadeInUp">
    <div class="container">
            <!-- <h1 class="text-center">{{ $language_id === 1 ? "Trường học tiêu biểu" : "" }}</h1> -->
            <div class="clientSA-heading">
					<h2>Trường học tiêu biểu</h2>
					<div class="agileits-line"> </div>
				</div>
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme clientSA-item">
                    <div class="logo-image item">
                        <a href="https://www.gaikokujinnavi.com/vn/schools/223" target="_">
                            <img src="citynow/images/page/citynowsa/school1.jpg" alt="" >
                        </a>
                    </div>
                    <div class="logo-image item">
                        <a href="https://www.gaikokujinnavi.com/vn/schools/210" target="_">
                            <img src="citynow/images/page/citynowsa/school2.jpg" alt="" >
                        </a>
                    </div>
                    <div class="logo-image item">
                        <a href="https://www.gaikokujinnavi.com/vn/schools/227" target="_">
                            <img src="citynow/images/page/citynowsa/school3.jpg" alt="" >
                        </a>
                    </div>
                    <div class="logo-image item">
                        <a href="https://www.gaikokujinnavi.com/vn/schools/224" target="_">
                            <img src="citynow/images/page/citynowsa/school4.jpg" alt="" >
                        </a>
                    </div>
              </div>
            </div>
        <!-- </div> -->     
        <div class="clearfix"></div>
    </div>
</section>
<section id="contact" class="wow fadeInUp">
	<div class="map">  
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.53416276843!2d106.66937981520617!3d10.770339692325622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ede6b4f4837%3A0xd0b43c7f61d6991e!2zMjk4IMSQxrDhu51uZyAzLzIsIFBoxrDhu51uZyAxMiwgUXXhuq1uIDEwLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1537243112609"></iframe>
		<div class="address agileits">
			<div class="w3ls-title">
				<h3>THÔNG TIN LIÊN HỆ</h3> 
			</div>
			<p>Address: 298 Lê Hồng Phong, Phường 12, Quận 10, Tp. HCM, Việt Nam</p>
			<p>Telephone: 028 38658926</p>
			<p>FAX: </p>
			<p>Email: <a href="mailto:corp@citynow.">corp@citynow.vn</a></p>
		</div> 
	</div>
</section>
@endsection
@section('script')
$(document).ready(function() {
      var owl = $('.clientSA-item');
      owl.owlCarousel({
        margin: 20,
        nav: true,
        loop: true,
        autoplay: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 4
          }
        }
      })
    })
@endsection

