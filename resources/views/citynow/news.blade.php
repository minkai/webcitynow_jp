@extends('citynow.master')
@section('content')
    <div id="news-detail">
        <div class="custom-container">
            <div class="title" style="background: rgba(0, 0, 0, 0.3); color: white; background-repeat: no-repeat; background-color: #082c44; background-size: auto; background-position: center;">
                {!! $news->title !!}
            </div>
            <div class="news-image">
                <img src="{{ $news->image_url }}">
            </div>
            <div class="location">
                <div>
                    <i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
                    {{ date('d-m-Y', strtotime($news->created_at)) }}
                </div>
            </div>
            <div class="content">
                {!! $news->content !!}
            </div>
        </div>
    </div>
@endsection