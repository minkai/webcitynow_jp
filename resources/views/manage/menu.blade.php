<div id="menu">
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav in" id="side-menu">
                <li>
                    <a class="language" href="language">Languages</a>
                </li>
                <li>
                    <a class="menu" href="menu">Menu</a>
                </li>
                <li>
                    <a class="footer" href="footer">Footer</a>
                </li>
                <li>
                    <a class="slide" href="slide">Slides</a>
                </li>
                <li>
                    <a class="introduction" href="introduction">Introduction</a>
                </li>
                <li>
                    <a class="manage-news" href="manage-news">News</a>
                </li>
                <li>
                    <a class="manage-customer" href="manage-customer">Customers</a>
                </li>
                <li>
                    <a class="recuitment" href="recuitment">Recuitments</a>
                </li>
                <li>
                    <a class="manage-service" href="manage-service">Services</a>
                </li>
                <li>
                    <a class="manage-welfare" href="manage-welfare">Welfare</a>
                </li>
                <li>
                    <a class="manage-team-testimonial" href="manage-team-testimonial">Team Testimonial</a>
                </li>
                <li>
                    <a class="manage-applicant" href="manage-applicant">Applicants</a>
                </li>
                <li>
                    <a class="manage-user" href="manage-user">Users</a>
                </li>
                <li>
                    <a class="manage-role" href="manage-role">Roles</a>
                </li>
            </ul>
        </div>
    </div>
</div>