@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-welfare')}}">
            <i class="fa fa-plus"></i>
            Add New Welfare
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($welfares as $index => $welfare)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$welfare->image_url}}" width="100"></td>
                    <td>{{$welfare->language->name}}</td>
                    <td>{{$welfare->name}}</td>
                    <td>{!! str_limit($welfare->description, $limit = 100, $end = '...') !!}</td>
                    <td>{{$welfare->order_number}}</td>
                    <td>
                        <a href="manage-welfare/edit/{{ $welfare->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-welfare/delete/{{ $welfare->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$welfares->links()}}
    </div>
@endsection