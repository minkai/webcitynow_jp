@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('recuitments') }}">Back to list</a>
            <form action="{{ route('add-recuitment') }}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('success_message'))
                        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                    @endif
                    <h4>Add New Recuitment</h4>
                    <div class="space20">&nbsp;</div>
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}">{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Title*</label>
                        <input type="text" class="form-control" name="title" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Type *</label>
                        <input type="text" class="form-control" name="type" required>
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary*</label>
                        <input type="text" class="form-control" name="salary" required>
                    </div>
                    <div class="form-group">
                        <label for="location">Location*</label>
                        <input type="text" class="form-control" name="location" required>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date*</label>
                        <div class='input-group date' id='start_date'>
                            <input type='text' class="form-control" name="start_date" data-date-format="dd MM yyyy" data-link-format="mm-dd-yyyy" required/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date*</label>
                        <div class='input-group date' id='start_date'>
                            <input type='text' class="form-control" name="end_date" required />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="job_description">Job Description*</label>
                        <textarea id="job_description" rows="8" cols="50" required class="form-control editor" name="job_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="job_requirement">Job Requirement*</label>
                        <textarea id="job_requirement" rows="8" cols="50" required class="form-control editor" name="job_requirement"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="job_benefit">Job Benefit*</label>
                        <textarea id="job_benefit" rows="8" cols="50" required class="form-control editor" name="job_benefit"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection
@section('script')

    bindDatePicker();
@endsection