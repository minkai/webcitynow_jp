@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-recuitment')}}">
            <i class="fa fa-plus"></i>
            Add New Recuitment
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Language</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Salary</th>
                <th scope="col">Location</th>
                <th scope="col">Start Date</th>
                <th scope="col">End Date</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($recuitments as $index => $recuit)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$recuit->language->name}}</td>
                    <td>{{$recuit->title}}</td>
                    <td>{{$recuit->type}}</td>
                    <td>{{$recuit->salary}}</td>
                    <td>{{$recuit->location}}</td>
                    <td>{{$recuit->start_date}}</td>
                    <td>{{$recuit->end_date}}</td>
                    <td>
                        <a href="recuitment/edit/{{ $recuit->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="recuitment/delete/{{ $recuit->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$recuitments->links()}}
    </div>
@endsection