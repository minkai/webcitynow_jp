@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('recuitments') }}">Back to list</a>
            <form action="{{route('edit-recuitment',$recuitment->id)}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    <h4>Edit Recuitment</h4>
                    <div class="space20">&nbsp;</div>
                    <input type="hidden" name="id" value="{{$recuitment->id}}">
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}" {{ ($recuitment->language_id == $pt->id) ? 'selected' : '' }}>{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Title*</label>
                        <input type="text" class="form-control" name="title" value="{{$recuitment->title}}" required>
                    </div>
                    <div class="form-group">
                        <label for="type">Type *</label>
                        <input type="text" class="form-control" name="type" value="{{$recuitment->type}}" required>
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary*</label>
                        <input type="text" class="form-control" name="salary" value="{{$recuitment->salary}}" required>
                    </div>
                    <div class="form-group">
                        <label for="location">Location*</label>
                        <input type="text" class="form-control" name="location" value="{{$recuitment->location}}" required>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date*</label>
                        {{--<input type="text" class="form-control" name="start_date" value="{{$recuitment->start_date}}" required>--}}
                        <div class='input-group date' id='start_date'>
                            <input type='text' class="form-control" name="start_date" value="{{$recuitment->start_date}}" required />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date*</label>
                        {{--<input type="text" class="form-control" name="end_date" value="{{$recuitment->end_date}}" required>--}}
                        <div class='input-group date' id='end_date'>
                            <input type='text' class="form-control" name="end_date" value="{{$recuitment->end_date}}" required />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="job_description">Job Description*</label>
                        <textarea id="job_description" rows="8" cols="50" required class="form-control" name="job_description">
                            {{$recuitment->job_description}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="job_requirement">Job Requirement*</label>
                        <textarea id="job_requirement" rows="8" cols="50" required class="form-control" name="job_requirement">
                            {{$recuitment->job_requirement}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="job_benefit">Job Benefit*</label>
                        <textarea id="job_benefit" rows="8" cols="50" required class="form-control" name="job_benefit">
                            {{$recuitment->job_benefit}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection
@section('script')
    {{--CKEDITOR.replace('job_description', { height: 100 });--}}
    {{--CKEDITOR.replace('job_requirement', { height: 100 });--}}
    {{--CKEDITOR.replace('job_benefit', { height: 100 });--}}
    ClassicEditor.create( document.querySelector( '#job_description' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
    ClassicEditor.create( document.querySelector( '#job_requirement' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
    ClassicEditor.create( document.querySelector( '#job_benefit' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
    bindDatePicker();
@endsection