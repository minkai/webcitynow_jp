@extends('manage.master')

@section('content')
    @if(Session::has('success'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-user')}}">
            <i class="fa fa-plus"></i>
            Add New User
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Phone</th>
                <th scope="col">Address</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $index => $u)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$u->name}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->role->name}}</td>
                    <td>{{$u->phone}}</td>
                    <td>{{$u->address}}</td>
                    <td>
                        <a href="manage-user/edit/{{ $u->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-user/delete/{{ $u->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$users->links()}}
    </div>
@endsection