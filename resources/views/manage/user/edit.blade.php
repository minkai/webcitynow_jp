@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('manage-user') }}">Back to list</a>
        <form action="{{route('edit-user',$user->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            @if(count($errors)>0)
                <div class="alert alert-danger pt10">
                    @foreach($errors->all() as $err)
                        {{$err}}
                    @endforeach
                </div>
            @endif
            <h4>Edit User</h4>
            <div class="space20">&nbsp;</div>
            <input type="hidden" name="id" value="{{$user->id}}">
            <div class="form-group">
                <label for="name">Full Name*</label>
                <input type="text" class="form-control" name="name" value="{{$user->name}}" required>
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input type="email" class="form-control" name="email" value="{{$user->email}}" required>
            </div>
            <div class="form-group">
                <label for="role_id">Role</label>
                <select class="form-control" name="role_id">
                    @foreach($roles as $r)
                        <option value="{{$r->id}}" {{ ($r->id === $user->role_id) ? 'selected' : '' }}>{{$r->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="phone_number">Phone*</label>
                <input type="text" class="form-control" name="phone_number" value="{{$user->phone}}" required>
            </div>
            <div class="form-group">
                <label for="address">Address*</label>
                <input type="text" class="form-control" name="address" value="{{$user->address}}" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div> <!-- #content -->
@endsection