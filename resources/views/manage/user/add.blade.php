@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('manage-user') }}">Back to list</a>
        <form action="{{route('add-user')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
                @if(count($errors)>0)
                    <div class="alert alert-danger pt10">
                        @foreach($errors->all() as $err)
                            {{$err}}
                        @endforeach
                    </div>
                @endif
                @if(Session::has('success_message'))
                    <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                @endif
                <h4>Add New User</h4>
                <div class="space20">&nbsp;</div>
                <div class="form-group">
                    <label for="name">Full Name*</label>
                    <input type="text" class="form-control" name="name" required>
                </div>
                <div class="form-group">
                    <label for="email">Email*</label>
                    <input type="email" class="form-control" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password*</label>
                    <input type="password" class="form-control" name="password" required>
                </div>
                <div class="form-group">
                    <label for="re_password">Confirm Password*</label>
                    <input type="password" class="form-control" name="re_password" required>
                </div>
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select class="form-control" name="role_id">
                        @foreach($roles as $r)
                            <option value="{{$r->id}}">{{$r->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="phone_number">Phone*</label>
                    <input type="text" class="form-control" name="phone_number" required>
                </div>

                <div class="form-group">
                    <label for="address">Address*</label>
                    <input type="text" class="form-control" name="address" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div> <!-- #content -->
@endsection