@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-customer')}}">
            <i class="fa fa-plus"></i>
            Add New Customer
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Name</th>
                <th scope="col">Link</th>
                <th scope="col">Content</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $index => $intro)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$intro->image_url}}" width="100"></td>
                    <td>{{$intro->language->name}}</td>
                    <td>{{$intro->name}}</td>
                    <td>{{$intro->link}}</td>
                    <td>{!! str_limit(strip_tags($intro->introduction), $limit = 100, $end = '...') !!}</td>
                    <td>{{$intro->order_number}}</td>
                    <td>
                        <a href="manage-customer/edit/{{ $intro->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-customer/delete/{{ $intro->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$customers->links()}}
    </div>
@endsection