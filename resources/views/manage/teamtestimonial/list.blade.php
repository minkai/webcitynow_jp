@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-team-testimonial')}}">
            <i class="fa fa-plus"></i>
            Add New Team Testimonial
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Name</th>
                <th scope="col">Position</th>
                <th scope="col">Location</th>
                <th scope="col">Description</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($teamtestimonials as $index => $teamtestimonial)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$teamtestimonial->image_url}}" width="100"></td>
                    <td>{{$teamtestimonial->language->name}}</td>
                    <td>{{$teamtestimonial->name}}</td>
                    <td>{{$teamtestimonial->position}}</td>
                    <td>{{$teamtestimonial->location}}</td>
                    <td>{!! str_limit(strip_tags($teamtestimonial->description), $limit = 100, $end = '...') !!}</td>
                    <td>{{$teamtestimonial->order_number}}</td>
                    <td>
                        <a href="manage-team-testimonial/edit/{{ $teamtestimonial->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-team-testimonial/delete/{{ $teamtestimonial->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$teamtestimonials->links()}}
    </div>
@endsection