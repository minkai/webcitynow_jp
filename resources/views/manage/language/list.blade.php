@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-language')}}">
            <i class="fa fa-plus"></i>
            Add New Language
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Language</th>
                <th scope="col">Alias</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($languages as $index => $lang)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$lang->name}}</td>
                    <td>{{$lang->alias}}</td>
                    <td>
                        <a href="language/edit/{{ $lang->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="language/delete/{{ $lang->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$languages->links()}}
    </div>
@endsection