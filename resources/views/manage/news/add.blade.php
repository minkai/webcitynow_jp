@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('manage-news') }}">Back to list</a>
            <form action="{{route('add-news')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('success_message'))
                        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                    @endif
                    @if(Session::has('success_fail'))
                        <div class="alert alert-danger pt10">{{Session::get('success_fail')}}</div>
                    @endif
                    <h4>Add New News</h4>
                    <div class="space20">&nbsp;</div>
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}">{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="title">Title*</label>
                        <input type="text" class="form-control" name="title" required>
                    </div>
                    <div class="form-group">
                        <label for="content_text">Content*</label>
                        <textarea id="editor" rows="10" name="content_text"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file_image">Image Url*</label>
                        <div class="input-group input-file" name="file_image">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-choose" type="button">Choose</button>
                            </span>
                            <input type="text" class="form-control" placeholder='Choose a file...'/>
                            <span class="input-group-btn">
                             <button class="btn btn-warning btn-reset" type="button">Reset</button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection
@section('script')
    {{--initSample();--}}
    bs_required_input_file();
    ClassicEditor.create( document.querySelector( '#editor' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
@endsection