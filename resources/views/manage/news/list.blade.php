@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-news')}}">
            <i class="fa fa-plus"></i>
            Add New News
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Title</th>
                <th scope="col">Content</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($news as $index => $n)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$n->image_url}}" width="100"></td>
                    <td>{{$n->language->name}}</td>
                    <td>{{$n->title}}</td>
                    <td>{!! str_limit(strip_tags($n->content), $limit = 100, $end = '...') !!}</td>
                    <td>
                        <a href="manage-news/edit/{{ $n->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-news/delete/{{ $n->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$news->links()}}
    </div>
@endsection