<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>City Now </title>
    <base href="{{asset('')}}">
    <link rel="stylesheet" href="citynow/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="citynow/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="citynow/assets/css/colorbox.css">
    <link rel="stylesheet" href="citynow/assets/bxslider/bxslider.css">
    <link rel="stylesheet" href="citynow/assets/css/spacing.css">
    <link rel="stylesheet" href="citynow/assets/css/style.css">
    <link rel="stylesheet" href="citynow/assets/css/manage.css">
    <link rel="stylesheet" href="citynow/assets/css/bootstrap-datetimepicker.css">
<!--     <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
      tinymce.init({
        selector: '#job_benefit'
      });
      </script> -->
</head>
<body>
<div class="custom-container">
    <div class="manage">
        <div class="header row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <a href="{{route('home')}}"><img src="citynow/images/logo/logovn.png" alt="" width="80%"></a>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="user-info">
                    @if (Illuminate\Support\Facades\Auth::check())
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Illuminate\Support\Facades\Auth::user()->name}}<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('/manage/change-password') }}">Change Password</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/manage/logout') }}">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
            {{--<div class="clearfix"></div>--}}
        </div>
        <div class="col-md-2 col-sm-2 col-xs-4" style="padding-left: 0px;">
            @include('manage.menu')
        </div>
        <div class="col-md-10 col-sm-10 col-xs-8" style="padding-right: 0px;">
            @yield('content')
        </div>
        {{--<div class="clearfix"></div>--}}
    </div>
</div> <!-- .container -->
<script src="citynow/assets/js/jquery.min.js"></script>
<script src="citynow/assets/js/bootstrap.min.js"></script>
<script src="citynow/assets/js/jquery.colorbox-min.js"></script>
<script src="citynow/assets/js/ckeditor5/ckeditor.js"></script>
<script src="citynow/assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<script src="citynow/assets/js/manage.js"></script>
<script src="citynow/assets/js/datepicker.js"></script>
<script>
    @yield('script')
    $(document).ready(function($) {
        $(".alert-success, .alert-danger").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert-success, .alert-danger").slideUp(500);
        });
        var url = window.location.pathname;
        var lstUrl = url.split('/');
        $('#side-menu li').find('.' + lstUrl[1]).addClass('active');
    });
</script>
</body>
</html>
