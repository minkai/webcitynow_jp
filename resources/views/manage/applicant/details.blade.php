@extends('manage.master')
@section('content')
    <div id="content">
            <form action="{{route('manage-applicant')}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row col-md-12">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    <h4>Applicant Details</h4>
                    <div class="space20">&nbsp;</div>
                    <input type="hidden" name="id" value="{{$applicant->id}}">
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id" disabled="disabled">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}" {{ ($applicant->language_id == $pt->id) ? 'selected' : '' }}>{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{$applicant->name}}" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="link">Email</label>
                        <input type="text" class="form-control" name="name" value="{{$applicant->email}}" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label for="introduction">Content*</label>
                        <textarea id="editor" name="introduction" disabled="disabled">{{$applicant->introduction}}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Back to list</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection
@section('script')
    {{--initSample('editor');--}}
    bs_input_file();
    ClassicEditor.create( document.querySelector( '#editor' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
@endsection