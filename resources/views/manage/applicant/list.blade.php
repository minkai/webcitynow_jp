@extends('manage.master')
@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Language</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Content</th>
                <th scope="col">Date</th>
                <th scope="col">CV</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($applicants as $index => $applicant)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$applicant->language->name}}</td>
                    <td>{{$applicant->name}}</td>
                    <td>{{$applicant->email}}</td>
                    <td>{!! $applicant->introduction !!}</td>
                    <td>{{ date('d-m-Y', strtotime($applicant->created_at)) }}</td>
                    <td><a href="{{ url($applicant->attachment_url) }}">Download</a></td>
                    <td>
                        <a href="manage-applicant/details/{{ $applicant->id }}">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-applicant/delete/{{ $applicant->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$applicants->links()}}
    </div>
@endsection