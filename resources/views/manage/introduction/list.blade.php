@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-introduction')}}">
            <i class="fa fa-plus"></i>
            Add New Introductions
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Page</th>
                <th scope="col">Title</th>
                <th scope="col">Content</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($introductions as $index => $intro)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$intro->image_url}}" width="100"></td>
                    <td>{{$intro->language->name}}</td>
                    <td>{{$intro->page}}</td>
                    <td>{{$intro->title}}</td>
                    <td>{!! str_limit(strip_tags($intro->content_text), $limit = 100, $end = '...') !!}</td>
                    <td>{{$intro->order_number}}</td>
                    <td>
                        <a href="introduction/edit/{{ $intro->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="introduction/delete/{{ $intro->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$introductions->links()}}
    </div>
@endsection