@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt10">
        <a class="beta-btn primary" href="{{route('add-menu')}}">
            <i class="fa fa-plus"></i>
            Add New Menu
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Language</th>
                <th scope="col">Name</th>
                <th scope="col">Alias</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $index => $menu)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$menu->language->name}}</td>
                    <td>{{$menu->name}}</td>
                    <td>{{$menu->alias}}</td>
                    <td>{{$menu->order_number}}</td>
                    <td>
                        <a href="menu/edit/{{ $menu->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="menu/delete/{{ $menu->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$menus->links()}}
    </div>
@endsection