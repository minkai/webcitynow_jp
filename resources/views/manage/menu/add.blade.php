@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('menu') }}">Back to list</a>
            <form action="{{route('add-menu')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('success_message'))
                        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                    @endif
                    <h4>Add New Menu</h4>
                    <div class="space20">&nbsp;</div>
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}">{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name*</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="order_number">Order*</label>
                        <input type="number" class="form-control" name="order_number" required>
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias*</label>
                        <input type="text" class="form-control" name="alias" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection