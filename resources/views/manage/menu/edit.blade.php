@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('menu') }}">Back to list</a>
            <form action="{{route('edit-menu',$menu->id)}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    <h4>Edit Menu</h4>
                    <div class="space20">&nbsp;</div>
                    <input type="hidden" name="id" value="{{$menu->id}}">
                    <div class="form-group">
                        <label for="language">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}" {{ ($menu->language_id == $pt->id) ? 'selected' : '' }}>{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name*</label>
                        <input type="text" class="form-control" name="name" value="{{$menu->name}}" required>
                    </div>
                    <div class="form-group">
                        <label for="alias">Alias*</label>
                        <input type="text" class="form-control" name="alias" value="{{$menu->alias}}" required>
                    </div>
                    <div class="form-group">
                        <label for="order_number">Order*</label>
                        <input type="number" class="form-control" name="order_number" value="{{$menu->order_number}}" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection