@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-footer')}}">
            <i class="fa fa-plus"></i>
            Add New Footer
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Language</th>
                <th scope="col">Title VN</th>
                <th scope="col">Address VN</th>
                <th scope="col">Phone VN</th>
                <th scope="col">Email VN</th>
                <th scope="col">Title JP</th>
                <th scope="col">Address JP</th>
                <th scope="col">Phone JP</th>
                <th scope="col">Email JP</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($footers as $index => $f)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$f->language->name}}</td>
                    <td>{{$f->title_vn}}</td>
                    <td>{{$f->address_vn}}</td>
                    <td>{{$f->phone_vn}}</td>
                    <td>{{$f->email_vn}}</td>
                    <td>{{$f->title_jp}}</td>
                    <td>{{$f->address_jp}}</td>
                    <td>{{$f->phone_jp}}</td>
                    <td>{{$f->email_jp}}</td>
                    <td>
                        <a href="footer/edit/{{ $f->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="footer/delete/{{ $f->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$footers->links()}}
    </div>
@endsection