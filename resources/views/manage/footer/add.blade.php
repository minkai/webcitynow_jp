@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('footer') }}">Back to list</a>
            <form action="{{route('add-footer')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    @if(count($errors)>0)
                        <div class="alert alert-danger pt10">
                            @foreach($errors->all() as $err)
                                {{$err}}
                            @endforeach
                        </div>
                    @endif
                    @if(Session::has('success_message'))
                        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
                    @endif
                    <h4>Add New Footer</h4>
                    <div class="space20">&nbsp;</div>
                    <div class="form-group">
                        <label for="language_id">Language*</label>
                        <select class="form-control" name="language_id">
                            @foreach($languages as $pt)
                                <option value="{{$pt->id}}">{{$pt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="introduction">Description*</label>--}}
                        {{--<textarea rows="8" cols="50" class="form-control" name="introduction"></textarea>--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label for="link_instagram">Link Instagram*</label>
                        <input type="text" class="form-control" name="link_instagram" required>
                    </div>
                    <div class="form-group">
                        <label for="link_twitter">Link Twitter*</label>
                        <input type="text" class="form-control" name="link_twitter" required>
                    </div>
                    <div class="form-group">
                        <label for="link_facebook">Link Facebook*</label>
                        <input type="text" class="form-control" name="link_facebook" required>
                    </div>
                    <div class="form-group">
                        <label for="title_vn">Title VN*</label>
                        <input type="text" class="form-control" name="title_vn" required>
                    </div>
                    <div class="form-group">
                        <label for="address_vn">Address VN*</label>
                        <input type="text" class="form-control" name="address_vn" required>
                    </div>
                    <div class="form-group">
                        <label for="phone_vn">Phone VN*</label>
                        <input type="text" class="form-control" name="phone_vn" required>
                    </div>
                    <div class="form-group">
                        <label for="email_vn">Email VN*</label>
                        <input type="text" class="form-control" name="email_vn" required>
                    </div>
                    <div class="form-group">
                        <label for="title_jp">Title JP*</label>
                        <input type="text" class="form-control" name="title_jp" required>
                    </div>
                    <div class="form-group">
                        <label for="address_jp">Address JP*</label>
                        <input type="text" class="form-control" name="address_jp" required>
                    </div>
                    <div class="form-group">
                        <label for="phone_jp">Phone JP*</label>
                        <input type="text" class="form-control" name="phone_jp" required>
                    </div>
                    <div class="form-group">
                        <label for="email_jp">Email JP*</label>
                        <input type="text" class="form-control" name="email_jp" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- #content -->
@endsection