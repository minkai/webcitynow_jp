@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('slides') }}">Back to list</a>
        <form action="{{route('edit-slide',$slide->id)}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
                @if(count($errors)>0)
                    <div class="alert alert-danger pt10">
                        @foreach($errors->all() as $err)
                            {{$err}}
                        @endforeach
                    </div>
                @endif
                <h4>Edit Slide</h4>
                <div class="space20">&nbsp;</div>
                <input type="hidden" name="id" value="{{$slide->id}}">
                <div class="form-group">
                    <label for="language_id">Language*</label>
                    <select class="form-control" name="language_id">
                        @foreach($languages as $pt)
                            <option value="{{$pt->id}}">{{$pt->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Slide Name*</label>
                    <input type="text" class="form-control" name="name" value="{{$slide->name}}" required>
                </div>
                <div class="form-group">
                    <label for="order_number">Order*</label>
                    <input type="number" class="form-control" name="order_number" value="{{$slide->order_number}}" required>
                </div>
                <div class="form-group">
                    <label for="file_image">Image Url</label>
                    <div class="input-group input-file" name="file_image">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-choose" type="button">Choose</button>
                        </span>
                        <input type="text" class="form-control" placeholder='Choose a file...'/>
                        <span class="input-group-btn">
                             <button class="btn btn-warning btn-reset" type="button">Reset</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div> <!-- #content -->
@endsection
@section('script')
    bs_input_file();
@endsection