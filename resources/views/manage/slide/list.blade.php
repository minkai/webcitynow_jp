@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-slide')}}">
            <i class="fa fa-plus"></i>
            Add New Slide
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Slide Name</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($slides as $index => $u)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$u->image_url}}" width="100"></td>
                    <td>{{$u->language->name}}</td>
                    <td>{{$u->name}}</td>
                    <td>{{$u->order_number}}</td>
                    <td>
                        <a href="slide/edit/{{ $u->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="slide/delete/{{ $u->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$slides->links()}}
    </div>
@endsection