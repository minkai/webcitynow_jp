@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-role')}}">
            <i class="fa fa-plus"></i>
            Add New Role
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Role Name</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $index => $u)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td>{{$u->name}}</td>
                    <td>
                        <a href="manage-role/edit/{{ $u->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-role/delete/{{ $u->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$roles->links()}}
    </div>
@endsection