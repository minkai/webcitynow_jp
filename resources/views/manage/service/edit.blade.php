@extends('manage.master')
@section('content')
    <div id="content">
        <a href="{{ route('manage-service') }}">Back to list</a>
        <form action="{{route('edit-service',$service->id)}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
                @if(count($errors)>0)
                    <div class="alert alert-danger pt10">
                        @foreach($errors->all() as $err)
                            {{$err}}
                        @endforeach
                    </div>
                @endif
                <h4>Edit Service</h4>
                <div class="space20">&nbsp;</div>
                <input type="hidden" name="id" value="{{$service->id}}">
                <div class="form-group">
                    <label for="language_id">Language*</label>
                    <select class="form-control" name="language_id">
                        @foreach($languages as $pt)
                            <option value="{{$pt->id}}" {{ ($service->language_id == $pt->id) ? 'selected' : '' }}>{{$pt->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="title">Title*</label>
                    <input type="text" class="form-control" name="title" value="{{$service->title}}" required>
                </div>
                <div class="form-group">
                    <label for="introduction">Introduction*</label>
                    <textarea id="introduction" class="form-control" name="introduction" required>{{$service->introduction}}</textarea>
                </div>
                <div class="form-group">
                    <label for="content_text">Content*</label>
                    <textarea id="content_text" class="form-control" name="content_text" required>{{$service->content_text}}</textarea>
                </div>
                <div class="form-group">
                    <label for="order_number">Order*</label>
                    <input type="number" class="form-control" name="order_number" value="{{$service->order_number}}" required>
                </div>
                <div class="form-group">
                    <label for="file_image">Image Url</label>
                    <div class="input-group input-file" name="file_image">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-choose" type="button">Choose</button>
                        </span>
                        <input type="text" class="form-control" placeholder='Choose a file...'/>
                        <span class="input-group-btn">
                             <button class="btn btn-warning btn-reset" type="button">Reset</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div> <!-- #content -->
@endsection
@section('script')
    {{--CKEDITOR.replace('introduction');--}}
    {{--CKEDITOR.replace('content_text');--}}
    ClassicEditor.create( document.querySelector( '#introduction' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
    ClassicEditor.create( document.querySelector( '#content_text' ), {
    ckfinder: {
    uploadUrl: 'https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
    }
    }).then(editor => {}).catch(error => {});
    bs_input_file();
@endsection