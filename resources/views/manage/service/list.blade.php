@extends('manage.master')

@section('content')
    @if(Session::has('success_message'))
        <br>
        <div class="alert alert-success pt10">{{Session::get('success_message')}}</div>
    @endif
    <div class="pt20">
        <a class="beta-btn primary" href="{{route('add-service')}}">
            <i class="fa fa-plus"></i>
            Add New Service
        </a>
    </div>
    <br>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Language</th>
                <th scope="col">Title</th>
                <th scope="col">Introduction</th>
                <th scope="col">Order</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($services as $index => $cli)
                <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    <td><img src="{{$cli->image_url}}" width="100"></td>
                    <td>{{$cli->language->name}}</td>
                    <td>{{$cli->title}}</td>
                    <td>{!! str_limit(strip_tags($cli->introduction), $limit = 100, $end = '...') !!}</td>
                    <td>{{$cli->order_number}}</td>
                    <td>
                        <a href="manage-service/edit/{{ $cli->id }}">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a onclick="return confirm('Are you sure?')" href="manage-service/delete/{{ $cli->id }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$services->links()}}
    </div>
@endsection