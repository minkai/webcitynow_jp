<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footer', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->text('introduction')->nullable();
            $table->string('link_instagram', 255);
            $table->string('link_twitter', 255);
            $table->string('link_facebook', 255);
            $table->string('title_vn', 255);
            $table->string('address_vn', 255);
            $table->string('phone_vn', 20);
            $table->string('email_vn', 255);
            $table->string('title_jp', 255);
            $table->string('address_jp', 255);
            $table->string('phone_jp', 20);
            $table->string('email_jp', 255);
            $table->boolean('active');
            $table->integer('create_user_id')->nullable();
            $table->integer('updated_user_id')->nullable();
            $table->timestamps();
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('footer');
    }
}
