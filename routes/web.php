<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('home',[
    'as'=>'home',
    'uses'=>'CityNowController@getIndex'
]);
// citynowSA
Route::get('citynowsa',[
    'as'=>'citynowsa',
    'uses'=>'CityNowController@SAindex'
]);
// end citynowSA
Route::get('about',[
    'as'=>'about',
    'uses'=>'CityNowController@getAbout'
]);
Route::get('service',[
    'as'=>'service',
    'uses'=>'CityNowController@getService'
]);
Route::get('service/detail/{id}',[
    'as'=>'service-detail',
    'uses'=>'CityNowController@getServiceDetail'
]);
Route::get('news',[
    'as'=>'news',
    'uses'=>'CityNowController@getNews'
]);
Route::get('news/{id}',[
    'as'=>'news-detail',
    'uses'=>'CityNowController@getNewsDetail'
]);
Route::get('story',[
    'as'=>'story',
    'uses'=>'CityNowController@getStories'
]);
Route::get('story/detail/{id}',[
    'as'=>'story-detail',
    'uses'=>'CityNowController@getStoryDetail'
]);
Route::get('career',[
    'as'=>'career',
    'uses'=>'CityNowController@getCareer'
]);
Route::get('career2',[
    'as'=>'career2',
    'uses'=>'CityNowController@getCareer2'
]);
Route::get('career/job/{id}',[
    'as'=>'job',
    'uses'=>'CityNowController@getJob'
]);
Route::get('customers',[
    'as'=>'customers',
    'uses'=>'CityNowController@getCustomers'
]);
Route::post('apply-online',[
    'as'=>'apply-online',
    'uses'=>'CityNowController@postApplyOnline'
]);
Route::get('contact',[
    'as'=>'contact',
    'uses'=>'CityNowController@getContact'
]);

////////////////Manage //////////////

//Login
Route::group(['middleware' => 'web'], function() {
    Route::get('manage/login',[
        'as'=>'login',
        'uses'=>'ManageController@getLogin'
    ]);
    Route::post('manage/login',[
        'as'=>'login',
        'uses'=>'ManageController@postLogin'
    ]);
    Route::get('manage/logout',[
        'as'=>'logout',
        'uses'=>'ManageController@logout'
    ]);
    Route::get('manage/change-password',[
        'as'=>'change-password',
        'uses'=>'ManageController@getChangePassword'
    ]);
    Route::post('manage/change-password',[
        'as'=>'change-password',
        'uses'=>'ManageController@postChangePassword'
    ]);
});
//End Login

Route::middleware(['auth'])->group(function() {
    //Begin User
    Route::get('manage-user', [
        'as' => 'manage-user',
        'uses' => 'ManageController@getUsers'
    ]);
    Route::get('manage-user/add', [
        'as' => 'add-user',
        'uses' => 'ManageController@getAddUser'
    ]);
    Route::post('manage-user/add', [
        'as' => 'add-user',
        'uses' => 'ManageController@postAddUser'
    ]);
    Route::get('manage-user/edit/{id}', [
        'as' => 'edit-user',
        'uses' => 'ManageController@getEditUser'
    ]);
    Route::post('manage-user/edit/{id}', [
        'as' => 'edit-user',
        'uses' => 'ManageController@postEditUser'
    ]);
    Route::get('manage-user/delete/{id}', [
        'as' => 'delete-user',
        'uses' => 'ManageController@getDeleteUser'
    ]);
    //end user

    //Begin Menu
    Route::get('menu', [
        'as' => 'menu',
        'uses' => 'ManageController@getMenu'
    ]);
    Route::get('menu/add', [
        'as' => 'add-menu',
        'uses' => 'ManageController@getAddMenu'
    ]);
    Route::post('menu/add', [
        'as' => 'add-menu',
        'uses' => 'ManageController@postAddMenu'
    ]);
    Route::get('menu/edit/{id}', [
        'as' => 'edit-menu',
        'uses' => 'ManageController@getEditMenu'
    ]);
    Route::post('menu/edit/{id}', [
        'as' => 'edit-menu',
        'uses' => 'ManageController@postEditMenu'
    ]);
    Route::get('menu/delete/{id}', [
        'as' => 'delete-menu',
        'uses' => 'ManageController@getDeleteMenu'
    ]);
    //end menu

    //Begin language
    Route::get('language', [
        'as' => 'language',
        'uses' => 'ManageController@getLanguages'
    ]);
    Route::get('language/add', [
        'as' => 'add-language',
        'uses' => 'ManageController@getAddLanguage'
    ]);
    Route::post('language/add', [
        'as' => 'add-language',
        'uses' => 'ManageController@postAddLanguage'
    ]);
    Route::get('language/edit/{id}', [
        'as' => 'edit-language',
        'uses' => 'ManageController@getEditLanguage'
    ]);
    Route::post('language/edit/{id}', [
        'as' => 'edit-language',
        'uses' => 'ManageController@postEditLanguage'
    ]);
    Route::get('language/delete/{id}', [
        'as' => 'delete-language',
        'uses' => 'ManageController@getDeleteLanguage'
    ]);
    //end language

    //Begin Roles
    Route::get('manage-role', [
        'as' => 'manage-role',
        'uses' => 'ManageController@getRoles'
    ]);
    Route::get('manage-role/add', [
        'as' => 'add-role',
        'uses' => 'ManageController@getAddRole'
    ]);
    Route::post('manage-role/add', [
        'as' => 'add-role',
        'uses' => 'ManageController@postAddRole'
    ]);
    Route::get('manage-role/edit/{id}', [
        'as' => 'edit-role',
        'uses' => 'ManageController@getEditRole'
    ]);
    Route::post('manage-role/edit/{id}', [
        'as' => 'edit-role',
        'uses' => 'ManageController@postEditRole'
    ]);
    Route::get('manage-role/delete/{id}', [
        'as' => 'delete-role',
        'uses' => 'ManageController@getDeleteRole'
    ]);
    //end roles

    //Begin Slides
    Route::get('slide', [
        'as' => 'slides',
        'uses' => 'ManageController@getSlides'
    ]);
    Route::get('slide/add', [
        'as' => 'add-slide',
        'uses' => 'ManageController@getAddSlide'
    ]);
    Route::post('slide/add', [
        'as' => 'add-slide',
        'uses' => 'ManageController@postAddSlide'
    ]);
    Route::get('slide/edit/{id}', [
        'as' => 'edit-slide',
        'uses' => 'ManageController@getEditSlide'
    ]);
    Route::post('slide/edit/{id}', [
        'as' => 'edit-slide',
        'uses' => 'ManageController@postEditSlide'
    ]);
    Route::get('slide/delete/{id}', [
        'as' => 'delete-slide',
        'uses' => 'ManageController@getDeleteSlide'
    ]);
    //end slide

    //Begin Introduction
    Route::get('introduction', [
        'as' => 'introductions',
        'uses' => 'ManageController@getIntroductions'
    ]);
    Route::get('introduction/add', [
        'as' => 'add-introduction',
        'uses' => 'ManageController@getAddIntroduction'
    ]);
    Route::post('introduction/add', [
        'as' => 'add-introduction',
        'uses' => 'ManageController@postAddIntroduction'
    ]);
    Route::get('introduction/edit/{id}', [
        'as' => 'edit-introduction',
        'uses' => 'ManageController@getEditIntroduction'
    ]);
    Route::post('introduction/edit/{id}', [
        'as' => 'edit-introduction',
        'uses' => 'ManageController@postEditIntroduction'
    ]);
    Route::get('introduction/delete/{id}', [
        'as' => 'delete-introduction',
        'uses' => 'ManageController@getDeleteIntroduction'
    ]);
    //end slide

    //Begin News
    Route::get('manage-news', [
        'as' => 'manage-news',
        'uses' => 'ManageController@getNews'
    ]);
    Route::get('manage-news/add', [
        'as' => 'add-news',
        'uses' => 'ManageController@getAddNews'
    ]);
    Route::post('manage-news/add', [
        'as' => 'add-news',
        'uses' => 'ManageController@postAddNews'
    ]);
    Route::get('manage-news/edit/{id}', [
        'as' => 'edit-news',
        'uses' => 'ManageController@getEditNews'
    ]);
    Route::post('manage-news/edit/{id}', [
        'as' => 'edit-news',
        'uses' => 'ManageController@postEditNews'
    ]);
    Route::get('manage-news/delete/{id}', [
        'as' => 'delete-news',
        'uses' => 'ManageController@getDeleteNews'
    ]);
    //end news

    //Begin logo
    Route::get('logo', [
        'as' => 'logo',
        'uses' => 'ManageController@getLogo'
    ]);
    Route::get('logo/add', [
        'as' => 'add-logo',
        'uses' => 'ManageController@getAddLogo'
    ]);
    Route::post('logo/add', [
        'as' => 'add-logo',
        'uses' => 'ManageController@postAddLogo'
    ]);
    Route::get('logo/edit/{id}', [
        'as' => 'edit-logo',
        'uses' => 'ManageController@getEditLogo'
    ]);
    Route::post('logo/edit/{id}', [
        'as' => 'edit-logo',
        'uses' => 'ManageController@postEditLogo'
    ]);
    Route::get('logo/delete/{id}', [
        'as' => 'delete-logo',
        'uses' => 'ManageController@getDeleteLogo'
    ]);
    //end logo

    //Begin customer
    Route::get('manage-customer', [
        'as' => 'manage-customer',
        'uses' => 'ManageController@getCustomers'
    ]);
    Route::get('manage-customer/add', [
        'as' => 'add-customer',
        'uses' => 'ManageController@getAddCustomer'
    ]);
    Route::post('manage-customer/add', [
        'as' => 'add-customer',
        'uses' => 'ManageController@postAddCustomer'
    ]);
    Route::get('manage-customer/edit/{id}', [
        'as' => 'edit-customer',
        'uses' => 'ManageController@getEditCustomer'
    ]);
    Route::post('manage-customer/edit/{id}', [
        'as' => 'edit-customer',
        'uses' => 'ManageController@postEditCustomer'
    ]);
    Route::get('manage-customer/delete/{id}', [
        'as' => 'delete-customer',
        'uses' => 'ManageController@getDeleteCustomer'
    ]);
    //end customer

    //Begin recuitments
    Route::get('recuitment', [
        'as' => 'recuitments',
        'uses' => 'ManageController@getRecuitments'
    ]);
    Route::get('recuitment/add', [
        'as' => 'add-recuitment',
        'uses' => 'ManageController@getAddRecuitment'
    ]);
    Route::post('recuitment/add', [
        'as' => 'add-recuitment',
        'uses' => 'ManageController@postAddRecuitment'
    ]);
    Route::get('recuitment/edit/{id}', [
        'as' => 'edit-recuitment',
        'uses' => 'ManageController@getEditRecuitment'
    ]);
    Route::post('recuitment/edit/{id}', [
        'as' => 'edit-recuitment',
        'uses' => 'ManageController@postEditRecuitment'
    ]);
    Route::get('recuitment/delete/{id}', [
        'as' => 'delete-recuitment',
        'uses' => 'ManageController@getDeleteRecuitment'
    ]);
    //end recuitment

    //Begin Footer
    Route::get('footer', [
        'as' => 'footer',
        'uses' => 'ManageController@getFooter'
    ]);
    Route::get('footer/add', [
        'as' => 'add-footer',
        'uses' => 'ManageController@getAddFooter'
    ]);
    Route::post('footer/add', [
        'as' => 'add-footer',
        'uses' => 'ManageController@postAddFooter'
    ]);
    Route::get('footer/edit/{id}', [
        'as' => 'edit-footer',
        'uses' => 'ManageController@getEditFooter'
    ]);
    Route::post('footer/edit/{id}', [
        'as' => 'edit-footer',
        'uses' => 'ManageController@postEditFooter'
    ]);
    Route::get('footer/delete/{id}', [
        'as' => 'delete-footer',
        'uses' => 'ManageController@getDeleteFooter'
    ]);
    //end Footer

    //Begin Service
    Route::get('manage-service', [
        'as' => 'manage-service',
        'uses' => 'ManageController@getServices'
    ]);
    Route::get('manage-service/add', [
        'as' => 'add-service',
        'uses' => 'ManageController@getAddService'
    ]);
    Route::post('manage-service/add', [
        'as' => 'add-service',
        'uses' => 'ManageController@postAddService'
    ]);
    Route::get('manage-service/edit/{id}', [
        'as' => 'edit-service',
        'uses' => 'ManageController@getEditService'
    ]);
    Route::post('manage-service/edit/{id}', [
        'as' => 'edit-service',
        'uses' => 'ManageController@postEditService'
    ]);
    Route::get('manage-service/delete/{id}', [
        'as' => 'delete-service',
        'uses' => 'ManageController@getDeleteService'
    ]);
    //end Service

    //Begin Welfare
    Route::get('manage-welfare', [
        'as' => 'manage-welfare',
        'uses' => 'ManageController@getWelfares'
    ]);
    Route::get('manage-welfare/add', [
        'as' => 'add-welfare',
        'uses' => 'ManageController@getAddWelfare'
    ]);
    Route::post('manage-welfare/add', [
        'as' => 'add-welfare',
        'uses' => 'ManageController@postAddWelfare'
    ]);
    Route::get('manage-welfare/edit/{id}', [
        'as' => 'edit-welfare',
        'uses' => 'ManageController@getEditWelfare'
    ]);
    Route::post('manage-welfare/edit/{id}', [
        'as' => 'edit-welfare',
        'uses' => 'ManageController@postEditWelfare'
    ]);
    Route::get('manage-welfare/delete/{id}', [
        'as' => 'delete-welfare',
        'uses' => 'ManageController@getDeleteWelfare'
    ]);
    //end Welfare

    //Begin Team Testimonial
    Route::get('manage-team-testimonial', [
        'as' => 'manage-team-testimonial',
        'uses' => 'ManageController@getTeamTestimonials'
    ]);
    Route::get('manage-team-testimonial/add', [
        'as' => 'add-team-testimonial',
        'uses' => 'ManageController@getAddTeamTestimonial'
    ]);
    Route::post('manage-team-testimonial/add', [
        'as' => 'add-team-testimonial',
        'uses' => 'ManageController@postAddTeamTestimonial'
    ]);
    Route::get('manage-team-testimonial/edit/{id}', [
        'as' => 'edit-team-testimonial',
        'uses' => 'ManageController@getEditTeamTestimonial'
    ]);
    Route::post('manage-team-testimonial/edit/{id}', [
        'as' => 'edit-team-testimonial',
        'uses' => 'ManageController@postEditTeamTestimonial'
    ]);
    Route::get('manage-team-testimonial/delete/{id}', [
        'as' => 'delete-team-testimonial',
        'uses' => 'ManageController@getDeleteTeamTestimonial'
    ]);
    //end Team Testimonial

    //Begin Team Testimonial
    Route::get('manage-applicant', [
            'as' => 'manage-applicant',
            'uses' => 'ManageController@getApplicants'
        ]);
    Route::get('manage-applicant/details/{id}', [
        'as' => 'details-applicant',
        'uses' => 'ManageController@getApplicantDetails'
    ]);
    Route::get('manage-applicant/delete/{id}', [
        'as' => 'delete-applicant',
        'uses' => 'ManageController@getDeleteApplicant'
    ]);
    Route::post('manage-applicant', [
        'as' => 'manage-applicant',
        'uses' => 'ManageController@goToApplicant'
    ]);
    //end Team Testimonial
});

Route::get('/', function() {
    return redirect('/home');
});