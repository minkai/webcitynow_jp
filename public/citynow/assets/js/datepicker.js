var bindDatePicker = function() {
    var firstOpen = true;
    $('.date').datetimepicker({
        format: 'mm-dd-yyyy',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    }).on("dp.show", function(){});
};
