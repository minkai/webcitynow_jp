-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Aug 29, 2018 at 02:11 AM
-- Server version: 8.0.3-rc-log
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citynow`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `language_id`, `name`, `link`, `introduction`, `order_number`, `image_url`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nijibox', 'http://nijibox-recruit.jp/', '<p>Nijibox</p>', '1', 'citynow/images/page/clients/nijibox.jpg', 1, NULL, NULL, '2018-04-09 13:21:14', '2018-04-09 13:21:14'),
(2, 1, 'Revollet', 'https://revollet.io/', '<p>Revollet</p>', '2', 'citynow/images/page/clients/revollet_io.png', 1, NULL, NULL, '2018-04-09 13:22:11', '2018-04-09 13:22:11'),
(3, 1, 'Aiku', 'https://aiku.co.jp/', '<p>Aiku</p>', '3', 'citynow/images/page/clients/aiku_co_jp.png', 1, NULL, NULL, '2018-04-09 13:22:37', '2018-04-09 13:22:37'),
(4, 1, 'E Come True', 'http://www.e-cometrue.com/', '<p>E Come True</p>', '4', 'citynow/images/page/clients/e_cometrue_com.png', 1, NULL, NULL, '2018-04-09 13:23:36', '2018-04-09 13:23:36'),
(5, 1, 'Code Camp', 'https://codecamp.jp/', '<p>Code Camp</p>', '5', 'citynow/images/page/clients/505773101.png', 1, NULL, NULL, '2018-04-09 13:24:08', '2018-04-09 13:24:08'),
(6, 1, 'shinsengumihonbu', 'http://shinsengumihonbu.com/index.html', '<p>shinsengumihonbu</p>', '6', 'citynow/images/page/clients/shinsengumihonbu_com.png', 1, NULL, NULL, '2018-04-09 13:24:39', '2018-04-09 13:24:39');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `language_id`, `name`, `link`, `introduction`, `order_number`, `image_url`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nijibox', 'http://nijibox-recruit.jp/', '<p>Nijibox</p>', '1', 'citynow/images/page/customer/nijibox.jpg', 1, NULL, NULL, '2018-04-23 16:30:44', '2018-04-23 16:30:44'),
(2, 1, 'Aiku', 'https://aiku.co.jp/', '<p>Aiku</p>', '2', 'citynow/images/page/customer/aiku_co_jp.png', 1, NULL, NULL, '2018-04-27 14:48:49', '2018-04-27 14:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci,
  `link_instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_vn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_vn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_vn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_vn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_jp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_jp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_jp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_jp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `language_id`, `introduction`, `link_instagram`, `link_twitter`, `link_facebook`, `title_vn`, `address_vn`, `phone_vn`, `email_vn`, `title_jp`, `address_jp`, `phone_jp`, `email_jp`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'https://www.linkedin.com/company/citynow/', 'https://twitter.com/Citynowltd', 'https://www.facebook.com/citynow.vn/', 'Công ty TNHH Citynow', '298 Đường 3/2, Phường 12, Quận 10, TP. HCM, Việt Nam', '+84 8 3865 8926', 'corp@citynow.vn', 'Công ty cổ phần Citynow Asia', 'Ikeda Building 2F, 1-7-3 Irifune, Chuo-ku, Tokyo, 104-0042, JAPAN', '+81 90 6490 6327', 'corp@citynow.jp', 1, NULL, NULL, '2018-04-09 12:10:23', '2018-06-12 08:37:40'),
(2, 2, 'Citynow Asia 株式会社はCEOのPhan Tuan Taiによって設立され、2016年7月に発足しました。 彼は元々自然科学大学の学生でした。日本では, ８年間の留学期間があり、その後、4年間 グリー株式会社に在籍し、実務経験を積みました。その為ゲーム・プラットフォームのオペレーションやゲーム開発、ウェブ・サービス、モバイル・アプリケーション開発等に関する豊富な知識を有します。 日本で身に着けた基盤と強い信念を持って、ベトナムの情報技術を世界へ発信するため、海外で活躍するチャンスをベトナム人の若者に提供することを決意し、日本にてCitynow Asia株式会社及びベトナムのホーチミン市にてCitynow有限会社を設立しました。', 'https://www.linkedin.com/company/citynow/', 'https://twitter.com/Citynowltd', 'https://www.facebook.com/citynow.vn/', 'Citynow Co., Ltd.', '298 Đường 3/2, Phường 12, Quận 10, TP. HCM, Việt Nam', '+84 8 3865 8926', 'corp@citynow.vn', 'Citynow Asia Inc.', 'Ikeda Building 2F, 1-7-3 Irifune, Chuo-ku, Tokyo, 104-0042, JAPAN', '+81 90 6490 6327', 'corp@citynow.jp', 1, NULL, NULL, '2018-04-09 12:12:27', '2018-04-09 12:12:27');

-- --------------------------------------------------------

--
-- Table structure for table `introductions`
--

CREATE TABLE `introductions` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `introductions`
--

INSERT INTO `introductions` (`id`, `language_id`, `title`, `page`, `content_text`, `image_url`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tầm nhìn', 'home', '<p>Là công ty cung cấp dịch vụ Công nghệ thông tin và cung ứng nguồn nhân lực chất lương hàng đầu tai Việt Nam.</p>', 'citynow/images/page/introduction/Asset 4.png', 1, 1, NULL, NULL, '2018-04-09 13:08:02', '2018-06-12 08:48:32'),
(2, 1, 'Sứ mệnh', 'home', '<p>“Đưa công nghệ thông tin của người Việt gần hơn với thế giới, mang lai gia trị cao nhất cho công đồng”</p>', 'citynow/images/page/introduction/mission.png', 2, 1, NULL, NULL, '2018-04-09 13:08:45', '2018-06-12 10:11:28'),
(3, 1, 'Uy tín', 'home', '<p>Citynow luôn chọn đặt uy tín lên hàng đầu, bảo vệ chữ tín như nhiệm vụ sống còn.</p><p>Citynow luôn nỗ lực hết mình bảo đảm và thực hiện cam kết của mình với khach hàng, đối tác, nhân viên đặc biệt là cac cam kết về chất lương đầu ra.</p>', 'citynow/images/page/introduction/reputation.png', 3, 1, NULL, NULL, '2018-04-09 13:09:50', '2018-06-12 10:11:51'),
(4, 1, 'Tâm huyết', 'home', '<p>Citynow luôn tâm huy&ecirc;́t với t&acirc;́t cả những công việc nhằm mang lai gia trị cao nh&acirc;́t cho t&acirc;́t cả cac bên. Bằng t&acirc;́t cả trí, lực và trach nhiệm Citynow hi&ecirc;̉u rõ và hoàn thành sứ mệnh, mang lai gia trị cao nh&acirc;́t cho công đ&ocirc;̀ng.</p>', 'citynow/images/page/introduction/people.png', 4, 1, NULL, NULL, '2018-04-09 13:10:30', '2018-04-09 13:10:30'),
(5, 1, 'Con người', 'home', '<p>Citynow luôn coi trọng con người, đ&ecirc;̀ cao gia trị con người như là tài sản quý bau nh&acirc;́t; xây dựng môi trường làm việc chuyên nghiệp, năng đông, sang tao, đ&ocirc;̉i mới; thực hiện cac chính sach thu nhập, phúc lơi t&ocirc;́i ưu; tao cơ hôi đươc đào tao, phat tri&ecirc;̉n toàn diện và công bằng cho t&acirc;́t cả can bô công nhân viên.</p>', 'citynow/images/page/introduction/people-hi.png', 5, 1, NULL, NULL, '2018-04-09 13:11:11', '2018-04-09 13:11:11'),
(6, 1, 'Nguồn lực', 'home', '<p>học hàng đ&acirc;̀u như Đai học Khoa Học Tự Nhiên, Đai học Bach Khoa, Đai học Công Nghệ Thông Tin...</p>', 'citynow/images/page/introduction/people-hi.png', 6, 1, NULL, NULL, '2018-04-09 13:12:06', '2018-04-09 13:12:06'),
(7, 1, 'Môi trường làm việc tốt', 'home', '<p>Citynow Asia cung c&acirc;́p môi trường làm việc t&ocirc;́t nh&acirc;́t cho cac ban, đặc biệt là kỹ sư Công nghệ thông tin. Cac ban đươc trang bị thi&ecirc;́t bị và môi trường làm việc tiện lơi nh&acirc;́t, tao&nbsp;đi&ecirc;̀u kiện t&ocirc;́t nh&acirc;́t đ&ecirc;̉ phat huy khả năng và sức sang tao tư đó tao ra dịch vụ gia trị nh&acirc;́t&nbsp;cho khach hàng.</p>', 'citynow/images/page/introduction/improve-work-environment.png', 7, 1, NULL, NULL, '2018-04-09 13:14:07', '2018-04-09 13:14:07'),
(8, 1, 'Phát triển kỹ năng', 'home', '<p>Citynow Asia luôn có những lớp học trao đ&ocirc;̉i kinh nghiệm, kỹ năng vào cu&ocirc;́i tu&acirc;̀n và hôi thảo vào cu&ocirc;́i thang. Trong qua trình làm việc, t&acirc;́t cả mọi người luôn chia sẻ ki&ecirc;́n thức và kinh nghiệm, h&ocirc;̃ trơ t&ocirc;́i đa đ&ecirc;̉ cac ban có th&ecirc;̉ hoàn thành công việc môt cach t&ocirc;́t nh&acirc;́t.</p>', 'citynow/images/page/introduction/Skill.png', 8, 1, NULL, NULL, '2018-04-09 13:14:45', '2018-04-09 13:14:45'),
(9, 1, 'Các lợi ích khác', 'home', '<p>Với mức lương cao so với mặt bằng cùng cac ch&ecirc;́ đô phúc lơi, đãi ngô t&ocirc;́t, Citynow Asia mang đ&ecirc;́n ch&acirc;́t lương cuôc s&ocirc;́ng ngày càng t&ocirc;́t hơn cho cac ban với phương châm lơi ích&nbsp;của cac ban cũng chính là lơi ích của Citynow Asia.</p>', 'citynow/images/page/introduction/benefits.png', 9, 1, NULL, NULL, '2018-04-09 13:15:27', '2018-04-09 13:15:27'),
(10, 1, 'CUNG CẤP LẬP TRÌNH VIÊN, DU HỌC VÀ GIỚI THIỆU VIỆC LÀM', 'about_header', '<p>Citynow đươc thành lập và đi vào hoat đông tư thang 7/2016 dưới sự lãnh đao của CEO Phan Tuấn Tài. Anh là cựu sinh viên FIT, hơn 12 năm kinh nghiệm sống và làm việc tai Nhật bản, tưng công tac trong cac công ty game hàng đầu ở Nhật, và rất nhiều kinh nghiệm làm việc vận hành platform game, phat triển game, dịch vụ web, mobile application. Với sự nhiệt huyết của tuổi trẻ, sự đam mê về công nghệ và niềm tin của người Nhật, anh quyết định thành lập Citynow ở Hồ Chí Minh và Citynow Asia ở Tokyo Nhật Bản cùng tâm huyết nâng cao tấm vóc Công nghệ thông tin giữa Việt Nam với thế giới, đồng thời mang văn hóa cũng như con người Việt Nam ra thới giới thông qua cac dịch vụ giới thiệu việc làm, tư vấn du học và cung ứng lập trình viên, tao ra nhiều cơ hôi học tập và làm việc cho cac ban trẻ ở Việt Nam.</p>', 'citynow/images/page/introduction/bg-about.png', 1, 1, NULL, NULL, '2018-04-09 13:28:39', '2018-04-26 17:08:13'),
(11, 1, 'Cung cấp lập trình viên', 'about', '<p>CITYNOW hiện đang là đơn vị cung c&acirc;́p nhi&ecirc;̀u lập trình viên làm việc onsite và offshore&nbsp;tai Việt Nam cho cac công ty ph&acirc;̀n m&ecirc;̀m khu vực Châu Á, đặt biệt là cho Nhật Bản. CITYNOW cung c&acirc;́p cho khach hàng quý doanh nghiệp những lập trình viên JAVA, PHP, SCALA, Android, IOS, RUBY,... cao c&acirc;́p.</p>', 'citynow/images/page/introduction/people-hi.png', 1, 1, NULL, NULL, '2018-04-09 13:29:38', '2018-04-09 13:29:38'),
(12, 1, 'Phát triển ứng dụng di động, phần mềm', 'about', '<p>Chúng tôi cung c&acirc;́p dịch vụ tư v&acirc;́n và phat tri&ecirc;̉n của cac ứng dụng di đông trên t&acirc;́t cả cac n&ecirc;̀n tảng: IOS, Android, Blackberry, Windows Mobile. Chúng tôi cung c&acirc;́p cho ban cac giải phap toàn diện tư thi&ecirc;́t k&ecirc;́ ý tưởng đ&ecirc;́n thực hiện, ki&ecirc;̉m soat ch&acirc;́t lương, và đưa vào sử dụng; nâng c&acirc;́p và bảo trì cac ứng dụng.</p>', 'citynow/images/page/introduction/improve-work-environment.png', 2, 1, NULL, NULL, '2018-04-09 13:30:09', '2018-04-09 13:30:09'),
(13, 1, 'Dịch vụ tư vấn du học và giới thiệu việc làm', 'about', '<p>Chúng tôi cùng ban hi&ecirc;̉u và xac định tương lai và ngh&ecirc;̀ nghiệp của mình, mang lai cơ hôi học tập và thành công cho t&acirc;́t cả cac ban.</p>\r\n\r\n<p>Chúng tôi là c&acirc;̀u n&ocirc;́i vững chắc cho học sinh, sinh viên, ngu&ocirc;̀n nhân lực có trình đô chuyên môn tai Việt Nam sang Nhật đ&ecirc;̉ học tập, làm việc và đ&acirc;̀u tư.</p>', 'citynow/images/page/introduction/Vision.png', 3, 1, NULL, NULL, '2018-04-09 13:30:48', '2018-04-09 13:30:48'),
(15, 2, 'Test', 'about_header', '<p>Test</p>', 'citynow/images/page/introduction/people.png', 1, 1, NULL, NULL, '2018-04-12 16:55:23', '2018-04-12 16:55:23'),
(16, 2, 'Test', 'about', '<p>Test</p>', 'citynow/images/page/introduction/people.png', 1, 1, NULL, NULL, '2018-04-12 16:55:41', '2018-04-12 16:56:15'),
(17, 1, 'Sáng tạo', 'value', '<p>S&aacute;ng tạo, kh&ocirc;ng ngừng đổi mới, hướng tới sự ho&agrave;n mỹ trong c&ocirc;ng nghệ l&agrave; gi&aacute; trị cốt l&otilde;i.</p>', 'citynow/images/page/introduction/chienluoc2.png', 1, 1, NULL, NULL, '2018-04-13 11:59:22', '2018-04-19 16:07:30'),
(19, 1, 'Uy tín', 'value', '<p>Citynow lu&ocirc;n đặt uy t&iacute;n l&ecirc;n h&agrave;ng đầu, bảo vệ chữ t&iacute;n như nhiệm vụ sống c&ograve;n. C&ocirc;ng ty lu&ocirc;n nỗi lực hết m&igrave;nh bảo đảm v&agrave; thực hiện cam kết của m&igrave;nh với đối t&aacute;c, kh&aacute;ch h&agrave;ng.</p>', 'citynow/images/page/introduction/uytin2.png', 2, 1, NULL, NULL, '2018-04-19 16:23:58', '2018-04-19 16:23:58'),
(20, 1, 'Trách nhiệm', 'value', '<p>Citynow lu&ocirc;n &yacute; thức được tr&aacute;ch nhiệm trong từng sản phẩm v&agrave; dịch vụ của m&igrave;nh, sẵn s&agrave;ng đồng h&agrave;nh v&agrave; hỗ trợ kh&aacute;ch h&agrave;ng mọi thời điểm.</p>', 'citynow/images/page/introduction/trachnhiem2.png', 3, 1, NULL, NULL, '2018-04-19 16:25:01', '2018-04-19 16:25:01'),
(21, 1, 'Tận tuỵ', 'value', '<p>Ch&uacute;ng t&ocirc;i nỗ lực hết m&igrave;nh với từng sản phẩm, dịch vụ, sẵn s&agrave;ng lắng nghe v&agrave; chia sẻ với từng đối t&aacute;c, kh&aacute;ch h&agrave;ng.</p>', 'citynow/images/page/introduction/tantuy2.png', 4, 1, NULL, NULL, '2018-04-19 16:25:33', '2018-04-19 16:25:33'),
(22, 1, 'Tin tức mới nhất', 'news_header', '<p>Cập nhật tin tức mới nhất của Citynow</p>', 'citynow/images/page/introduction/banner002.png', 1, 1, NULL, NULL, '2018-04-23 08:33:42', '2018-04-26 16:52:10'),
(23, 1, 'Dịch vụ', 'service_header', '<p>Các dịch vụ của Citynow</p>', 'citynow/images/page/introduction/bg-career.png', 1, 1, NULL, NULL, '2018-04-23 08:34:54', '2018-04-26 16:18:00'),
(24, 1, 'Tuyển dụng', 'career_header', '<p>Tuyển dụng của Citynow</p>', 'citynow/images/page/introduction/bg-career.png', 1, 1, NULL, NULL, '2018-04-23 08:35:59', '2018-04-26 16:21:22'),
(25, 1, 'Khách hàng tiêu biểu', 'customer_header', '<p>Khách hàng tiêu biểu của Citynow</p>', 'citynow/images/page/introduction/bg-client.png', 1, 1, NULL, NULL, '2018-04-23 08:36:48', '2018-04-26 17:13:47'),
(26, 2, '典型的な顧客', 'customer_header', '<p>典型的な顧客</p>', 'citynow/images/page/introduction/001.jpg', 1, 1, NULL, NULL, '2018-04-23 08:37:19', '2018-04-23 08:37:19'),
(27, 2, '最新の情報', 'news_header', '<p>最新の情報</p>', 'citynow/images/page/introduction/001.jpg', 1, 1, NULL, NULL, '2018-04-23 08:37:53', '2018-04-23 08:37:53'),
(28, 2, 'サービス', 'service_header', '<p>サービス</p>', 'citynow/images/page/introduction/001.jpg', 1, 1, NULL, NULL, '2018-04-23 08:38:49', '2018-04-23 08:38:49'),
(29, 2, 'test', 'career_header', '<p>test</p>', 'citynow/images/page/introduction/001.jpg', 1, 1, NULL, NULL, '2018-04-23 11:25:23', '2018-04-23 11:25:23');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `alias`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Tiếng Việt', 'vi', 1, NULL, NULL, '2018-04-06 17:57:08', '2018-04-06 17:57:08'),
(2, '日本語', 'jp', 1, NULL, NULL, '2018-04-06 17:59:24', '2018-04-06 17:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `language_id`, `name`, `alias`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Về Chúng Tôi', 'about', 1, 1, NULL, NULL, '2018-04-06 18:00:10', '2018-04-06 18:00:10'),
(2, 1, 'Tuyển Dụng', 'career', 2, 1, NULL, NULL, '2018-04-06 18:01:46', '2018-04-11 14:18:17'),
(3, 1, 'Tin tức', 'news', 3, 1, NULL, NULL, '2018-04-06 18:02:17', '2018-04-06 18:02:17'),
(4, 1, 'Dịch vụ', 'service', 4, 1, NULL, NULL, '2018-04-06 18:03:32', '2018-04-06 18:03:32'),
(5, 1, 'Khách hàng tiêu biểu', 'customers', 5, 1, NULL, NULL, '2018-04-06 18:04:01', '2018-04-12 16:05:38'),
(6, 2, '会社情報', 'about', 1, 1, NULL, NULL, '2018-04-06 18:04:47', '2018-04-06 18:04:47'),
(8, 2, '採用情報', 'career', 2, 1, NULL, NULL, '2018-04-11 12:20:33', '2018-04-11 14:18:07'),
(9, 2, 'ニュース', 'news', 3, 1, NULL, NULL, '2018-04-11 12:21:46', '2018-04-11 13:38:30'),
(10, 2, 'サービス', 'service', 4, 1, NULL, NULL, '2018-04-11 12:24:12', '2018-04-11 13:38:39'),
(11, 2, 'クライアント', 'customers', 5, 1, NULL, NULL, '2018-04-11 12:24:49', '2018-04-12 16:52:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(112, '2018_01_19_145304_create_roles_table', 1),
(113, '2018_01_19_145320_create_users_table', 1),
(114, '2018_01_19_145511_create_languages_table', 1),
(115, '2018_01_19_145535_create_menus_table', 1),
(116, '2018_01_19_145558_create_slides_table', 1),
(117, '2018_01_19_145615_create_logos_table', 1),
(118, '2018_01_19_145656_create_clients_table', 1),
(119, '2018_01_19_145714_create_introductions_table', 1),
(120, '2018_01_19_145727_create_news_table', 1),
(121, '2018_01_19_145744_create_recruitments_table', 1),
(122, '2018_01_22_170508_create_footer_table', 1),
(123, '2018_01_24_150846_create_service_table', 1),
(124, '2018_04_06_093327_create_welfare_table', 1),
(125, '2018_04_06_093641_create_teamtestimonial_table', 1),
(126, '2018_04_12_151938_create_client_table', 2),
(127, '2018_04_12_151938_create_customer_table', 3),
(128, '2018_04_17_162753_create_applicant_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `language_id`, `title`, `content`, `image_url`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Teambuilding tháng 03/2018', '<p>Teambuilding đầu tiên khởi đầu cho năm mới vào ngày 02/03/2018 vừa qua, là một ngày khá bổ ích cho gia đình Citynow. Vừa là dịp mọi người cùng nhau quây quần, cùng nhau vui chơi, gắn kết mọi người trở nên thân thiết với nhau hơn. Teambuilding được tổ chức vào lúc 18h và địa điểm lần này là tại Citynow tạo cho mọi người không gian đầm ấm. Sau khi ăn uống và được lì xì lấy lộc đầu năm, mọi người cùng nhau chơi trò UNO và Ma sói không thể nào hại não hơn. Đó là một ngày không thể nào quên đối với gia đình Citynow!</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=teambuilding%281%29.JPG\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=teambuilding+%281%29%282%29.jpg\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=teambulding1%282%29.jpg\"></figure><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>', 'citynow/images/page/news/teambuilding.JPG', 1, NULL, NULL, '2018-03-02 11:18:53', '2018-04-24 16:39:21'),
(2, 1, 'Công ty TNHH Citynow phối hợp cùng Công Nghệ Thông Tin – Đại học Khoa học Tự Nhiên tổ chức Khóa học Kĩ năng viết CV và Phỏng vấn', '<p>Trong hai ngày 7 và 8/4/2018, Khóa học Kĩ Năng đã diễn ra tốt đẹp gồm hai chuyên đề là Viết CV và Phỏng vấn chinh phục nhà tuyển dụng. Chương trình với sự tham gia của thầy phụ trách Nguyễn Đức Hải – Giám đốc dịch vụ Top CV và hơn 100 sinh viên hệ Đào tạo Chất lượng cao của Khoa Công Nghệ Thông Tin.</p><p>Khóa học nhận được sự tương tác và phản hồi rất tích cực từ phía sinh viên trường Đại học Khoa Học Tự Nhiên, hứa hẹn xây dựng đội ngũ IT vững chuyên môn giỏi Kĩ năng, sẵn sàng góp phần rút ngắn khoảng cách công nghệ trong tương lai gần.</p><p>&nbsp;</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=seminar.jpg\"></figure>', 'citynow/images/page/news/hoithao.jpg', 1, NULL, NULL, '2018-04-08 11:19:20', '2018-04-26 18:00:52'),
(3, 2, 'CITYNOW有限会社は情報工科大学と連携して、日本語対応可能なIT技術者を育成することで合意しました。', '<p><strong>2017年9月7日情報工科大学において、Citynow有限会社と業務連携に関する覚書調印式が開催されました。</strong></p>\r\n\r\n<p>覚書調印式では、情報工科大学側に学校長のグエン・ホアン・チュ・アン博士、副校長のブ・ドック・ルン氏、<br />\r\nCitynow有限会社側に社長のファン・トゥアン・タイ氏、取締役社長のファン・ティ・ゴック氏が参加致しました。<br />\r\n調印式の来賓者として、株式会社イー・カムトゥルー社長の上田 正巳様、株式会社Revollet代表の櫛野隆太郎様、<br />\r\n株式会社 オープランズ社長の須貝 敦様にも参加頂きました。</p>\r\n\r\n<p><img alt=\"\" src=\"http://citynow.jp/wp-content/uploads/2017/09/1505230290_Edited1-1-300x165.png\" style=\"height:261px; width:475px\" /></p>\r\n\r\n<p>情報工科大学代表とCitynow有限会社社長の覚書調印式での様子</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>この調印式で、以下5つのことが決定しました。</p>\r\n\r\n<p>1．Citynow有限会社は情報工科大学と連携して、日本語対応可能なエンジニアを育成するプログラム（略JSIH）を実施。<br />\r\n2．JSIHプログラム内で日本語教師の提供することを決定。<br />\r\n3．JSIHプログラム内でオンライン日本語教育アプリを提供。<br />\r\n4．情報工科大学の学生にインターンシップと就職活動を支援。<br />\r\n5．JSIHプログラム内で日本での就職を希望する大学生に日本での仕事情報を提供。</p>\r\n\r\n<p>この調印式で、学校長のグエン・ホアン・チュ・アン博士は今回の業務連携を祝い、この業務連携が情報工科大学の学生にとって確かな利益をもたらすと述べています。</p>\r\n\r\n<p>Citynow有限会社 はCEOであるファン・トゥアン・タイ氏によって設立され、ファン・トゥアン・タイ氏は自然科学大学の元大学生、文部科学省から奨学金を頂き8年間日本に留学致しました。日本のインターネットサービス会社であるグリー株式会社に４年間勤務致しました。日本企業の仕事の進め方や日本での実績、高い技術レベルを活かして、ベトナム国内と世界のテクノロジーギャップを埋めるために、ホーチミン市にCitynow有限会社を設立致しました。</p>\r\n\r\n<p>現在、Citynow有限会社では、CourseActというオンライン日本語教育サービスや、Gaijinnaviという日本での就職を希望するエンジニアや日本語対応可能なベトナム人に日本国内の仕事を提供するサービスを運営しております。このサービスが、情報工科大学の学生にとって日本で働くチャンスになればと考えております。</p>\r\n\r\n<p>Citynow有限会社では今後も、情報工科大学と連携し、優秀な人材の育成に努め、この取組みを拡大していく予定です。</p>', 'citynow/images/page/news/news-jp.png', 1, NULL, NULL, '2018-04-11 12:37:58', '2018-04-11 12:37:58'),
(4, 1, 'Party ngày 8/3 của tập thể các bạn nữ Citynow', '<p>Nhằm tri ân cũng như tạo nên những bất ngờ ý nghĩa cho các bạn nữ trong công ty, CEO công ty cùng các nam IT đã bí mật tổ chức buổi Party với hoa và bánh ngọt.</p><p>Ngày 8/3 của các chị em công ty Citynow trở nên thật ấm áp và thú vị bởi những lời chúc và giây phút cùng nhau tại phòng họp lớn, tại đây các thành viên cũng không quên lưu lại những bức hình lưu niệm một cách vui vẻ và đầy tiếng cười…</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=IMG_0290.png\"></figure><p>&nbsp;</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=IMG_7302.JPG\"></figure><p>&nbsp;</p><p>&nbsp;</p>', 'citynow/images/page/news/IMG_7302.JPG', 1, NULL, NULL, '2018-03-08 17:10:45', '2018-04-24 17:24:58'),
(5, 1, 'Công ty TNHH Citynow và Công ty Ecomtrue hợp tác hỗ trợ nguồn nhân lực Việt Nam ra nước ngoài làm việc.', '<p>Hôm nay, ngày 20/11/2017 theo tờ Nhật báo Nikkei đăng tin hợp tác giữa công ty TNHH Citynow và công ty Ecomtrue sẽ bắt đầu các dịch vụ hỗ trợ về mặt nhân sự đưa người Việt Nam ra nước ngoài làm việc.</p><p>Theo Trích dẫn bài báo:</p><p>“Công ty Ecometrue phối hợp cùng với Citynow hỗ trợ đưa nguồn lao động cấp cao Việt Nam đến các doanh nghiệp hoạt động lĩnh vực nhà hàng tại Nhật Bản.</p><p>Công ty Ecometrue chuyên phát triển hệ thống quản lý chuỗi nhà hàng, từ tháng 12 sẽ bắt đầu các dịch vụ hỗ trợ về mặt nhân sự cho các doanh nghiệp nhà hàng tại Nhật. Công ty Ecomtrue và Citynow sẽ triển khai đưa người Việt Nam tốt nghiệp đại học, có trình độ tiếng Nhật từ N2 trở lên sang Nhật và đến các doanh nghiệp nhà hàng. Tại đây, họ sẽ làm việc với tư cách là một người quản lý tại các nhà hàng của chuỗi nhà hàng. Dự tính trong năm sau sẽ đưa khoảng 200 người sang Nhật làm việc.</p><p>Công ty Ecometrue sẽ liên kết với công ty Citynow ở TP.HCM. Công ty Citynow sẽ tuyển dụng nhân sự và công ty Ecometrue sẽ tiến hành phỏng vấn ở Nhật. Khi tuyển dụng, công ty Ecometrue sẽ hỗ trợ Visa lao động và đưa lao động vào doanh nghiệp Nhật. Những doanh nghiệp nhà hàng sẽ đào tạo kinh nghiệm quản lý cho người lao động tại trụ sở chính và các chuổi cửa hàng.</p><p>Ứng viên sẽ có thể được chuyển lên làm nhân viên chính thức hoặc nhân viên thời vụ. Sau đó sẽ được xem xét để đưa đến các chi nhánh khác ở nước ngoài như Myanma. Ngoài ra, công ty Ecometrue đảm nhiệm vai trò cung cấp nguồn nhân lực cho những công ty IT.”</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=Hinh+citynow+11_11.jpg\"></figure><p>&nbsp;</p>', 'citynow/images/page/news/Hinh citynow 11.11.jpg', 1, NULL, NULL, '2017-11-20 16:33:43', '2018-04-24 16:44:48'),
(6, 1, 'Team Building tháng 10/2017', '<p>Ngày 24/10/2017 Team Building tháng 10 được tổ chức tại King BBQ. Cũng nhân dịp này Citynow đã tổ chức mừng sinh nhật cho nhân viên có sinh nhật vào tháng 10 là chị Huỳnh Lê Kim Ngân – quản lý sản phẩm Gaijinnavi. Chúc chị luôn vui vẻ, mạnh khỏe, thành công trong công việc và cuộc sống. Cảm ơn những đóng góp của chị cho công ty trong thời gian qua và hy vọng chị sẽ tiếp tục kề vai sát cánh với Citynow trong những chặng đường sắp tới.</p><figure class=\"image\"><img src=\"http://citynow.jp/wp-content/uploads/2017/10/1509012399-300x225.jpg\" alt=\"\"></figure><p>&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;</p><figure class=\"image\"><img src=\"http://citynow.jp/wp-content/uploads/2017/10/1509012761-300x225.jpg\" alt=\"\"></figure><figure class=\"image\"><img src=\"http://citynow.jp/wp-content/uploads/2017/10/1509012250-300x225.jpg\" alt=\"\"></figure>', 'citynow/images/page/news/teambuild102017.jpg', 1, NULL, NULL, '2017-10-26 16:34:27', '2018-04-24 17:27:06'),
(7, 1, 'Lễ ký kết sự hợp tác giữa Công ty TNHH Citynow và Trường ĐH Công nghệ Thông tin', '<p><strong>Vào ngày 07/9/2017 tại Trường Đại học Công nghệ Thông tin (ĐH CNTT) đã diễn ra buổi lễ ký kết bản ghi nhớ hợp tác giữa Công ty TNHH Citynow.</strong></p><p>Tham dự lễ ký kết, về phía Trường ĐH CNTT có: TS Nguyễn Hoàng Tú Anh – Hiệu trưởng; PGS.TS Vũ Đức Lung – Phó Hiệu trưởng; lãnh đạo của các phòng ban, khoa.</p><p>Về phía Công ty Citynow có: Ông Phan Tuấn Tài – Chủ tịch Công ty Citynow, Bà Phan Thị Ngọc – Giám đốc Công ty Citynow và đại diện các công ty Nhật Bản: Ông Masami Ueda – Giám đốc Công ty Ecometrue, Ông Ryutaro Kushino – Đại diện Công ty Revollet, Ông Atsushi Uga Sugai – Giám đốc Công ty Oplans.</p><figure class=\"image\"><img src=\"http://citynow.jp/wp-content/uploads/2017/09/1505230290_Edited1-300x165.png\" alt=\"\"></figure><p><i>Đại diện Trường ĐH CNTT và chủ tịch Công ty Citynow trao ký kết thỏa thuận hợp tác</i></p><p>Hai bên đã ký kết hợp tác trong 4 lĩnh vực như sau:</p><ol><li>Cung cấp giảng viên giảng dạy tiếng Nhật cho chương trình đào tạo nhân lực CNTT có kỹ năng tiềng Nhật (viết tắt là JSIH), hợp tác giữa trường ĐH CNTT và công ty TNHH Citynow.</li><li>Hỗ trợ phần mềm dạy học trực tuyến và quản lý học vụ cho các chương trình JSIH.</li><li>Hỗ trợ thực tập chuyên môn và tạo cơ hội việc làm cho sinh viên chương trình JSIH.</li><li>Hỗ trợ sinh viên chương trình JSIH sau tốt nghiệp tìm cơ hội việc làm sang Nhật Bản.</li></ol><p>Tại buổi ký kết, TS Nguyễn Hoàng Tú Anh chúc mừng hai bên ký kết hợp tác và cho rằng việc hợp tác này sẽ đem lại nhiều lợi ích cho sinh viên Trường. Và Trường ĐH CNTT cũng đánh giá cao việc hợp tác với Công ty TNHH Citynow sẽ tạo ra nguồn nhân lực có kiến thức chuyên môn và kỹ năng ngoại ngữ tốt.</p><p>Công ty Citynow được thành lập bởi anh Phan Tuấn Tài, là cựu sinh viên trường Khoa học tự nhiên, có quá trình 8 năm du học với học bổng chính phủ Nhật và làm việc 4 năm trong công ty Gree, công ty cung cấp dịch vụ Internet hàng đầu ở Nhật Bản. Với nền tảng đã có và tâm huyết giảm khoảng cách CNTT giữa Việt Nam và thế giới anh đang điều hành công ty Citynow. Công ty Citynow hiện đang xây dựng và vận hành hai dịch vụ GaijinNavi – tư vấn du học, giới thiệu việc làm đi Nhật miễn phí và CourseAct – dịch vụ dạy học tiếng Nhật trực tuyến; sẽ hỗ trợ cho sinh viên chương trình JSIH &nbsp;để ra nước ngoài làm việc, vươn xa hơn trong biển lớn Quốc tế.</p>', 'citynow/images/page/news/news-jp.png', 1, NULL, NULL, '2017-09-12 16:35:10', '2018-04-24 17:28:15'),
(8, 1, 'Hội thảo \"Cơ hội nghề nghiệp dành cho cử nhân CNTT tại Nhật Bản\" tại trường Đại học Khoa Học Tự Nhiên', '<p>Ngày 13/03/2018, Công ty TNHH Citynow&nbsp;đã phối hợp cùng trường Đại học Khoa Học Tự Nhiên tổ chức buổi Hội thảo \"CƠ HỘI NGHỀ NGHIỆP DÀNH CHO CỬ NHÂN CNTT TẠI NHẬT BẢN\" nhằm giúp cho các bạn sinh viên ngành CNTT hiểu thêm hơn về đất nước con người cũng như môi trường làm việc tại Nhật Bản.</p><p>Trình bày tại buổi hội thảo đều là các nhân vật chủ chốt tại các công ty về CNTT tại Nhật Bản và Việt Nam như ông Phan Tuấn Tài – CEO của Công ty Citynow, ông Jun Maruyama - Đại diện Công ty Recruit Japan, ông Fujita - CEO Công ty DCWorks Japan, ông Fujihara - Đại diện Công ty Nijibox Japan. Phần trình bày của các diễn giả đã cung cấp rất nhiều thông tin về cuộc sống, học tập và làm việc tại Nhật Bản cũng như mở ra định hướng phát triển nghề nghiệp cho các bạn Cử nhân CNTT đã và đang có nguyện vọng làm việc tại Nhật Bản trong tương lai.</p><figure class=\"image\"><img src=\"https://cksource.com/weuy2g4ryt278ywiue/core/connector/php/connector.php?command=Proxy&amp;type=Files&amp;currentFolder=%2F&amp;fileName=IMG_4278_1.jpg\"></figure><p>&nbsp;</p>', 'citynow/images/page/news/IMG_4278_1.jpg', 1, NULL, NULL, '2018-03-13 17:08:50', '2018-04-24 17:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `recuitments`
--

CREATE TABLE `recuitments` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_requirement` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_benefit` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `recuitments`
--

INSERT INTO `recuitments` (`id`, `language_id`, `title`, `type`, `salary`, `location`, `start_date`, `end_date`, `job_description`, `job_requirement`, `job_benefit`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '6 Java Developers (800$-1,500$ net)', 'Full Time', 'Cạnh tranh', 'HCM, Việt Nam', '04-09-2018', '12-30-2018', '<p>Làm việc cùng với nhóm trong nước và nước ngoài. Chịu trách nhiệm với chất lượng của ứng dụng xây dựng triển khai trong suốt vòng đời sử dụng: • Phát triển, triển khai và bảo trì các ứng dụng web.&nbsp; • Mở rộng và customize các ứng dụng theo yêu cầu khách hàng • Phân tích, thiết kế, phát triển, kiểm thử và triển khai các ứng dụng web/website • Hỗ trợ và mở rộng các ứng dụng web/website • Có thể làm việc độc lập, tuân thủ kỷ luật và đáp ứng yêu cầu tiến độ</p>', '<p>Làm việc cùng với nhóm trong nước và nước ngoài. Chịu trách nhiệm với chất lượng của ứng dụng xây dựng triển khai trong suốt vòng đời sử dụng: • Phát triển, triển khai và bảo trì các ứng dụng web.&nbsp; • Mở rộng và customize các ứng dụng theo yêu cầu khách hàng • Phân tích, thiết kế, phát triển, kiểm thử và triển khai các ứng dụng web/website • Hỗ trợ và mở rộng các ứng dụng web/website • Có thể làm việc độc lập, tuân thủ kỷ luật và đáp ứng yêu cầu tiến độ</p>', '<p>• Mức lương net tùy theo kỹ năng và số năm kinh nghiệm: 800USD - 1,500USD • Thưởng lương tháng 13 cố định, thưởng theo hiệu quả đóng góp, xây dựng hoàn thành các dự án tham gia trong năm • Địa chỉ văn phòng: Tầng 6, tòa nhà 20/538 Đường Láng (sau tòa Skycity 88 Láng Hạ), Q. Đống Đa, HN • Thời gian làm việc: 8.30am-5.30pm (Thứ hai - Thứ sáu) và theo yêu cầu công việc • Đánh giá nâng hạng công việc hàng năm • Lương trả vào tài khoản • Tăng lương theo khả năng đóng góp • Được tham gia Training về công việc tại trụ sở chính công ty ở nước ngoài&nbsp; • Chế độ nghỉ phép: sinh con (cha hoặc mẹ), hoàn cảnh đặc biệt, kết hôn, thi chứng chỉ, thiên tai • Hỗ trợ BHYT, BHXH, BH sức khỏe bao gồm con cái, chế độ sản phụ&nbsp; • Phụ cấp hội thảo, công tác, đào tạo, du lịch, giải trí, team building theo quý, tiệc sinh nhật • Gửi CV bằng tiếng Anh theo địa chỉ email: hr@fitech.com.vn</p>', 1, NULL, NULL, '2018-04-09 13:34:47', '2018-06-14 10:04:46'),
(2, 2, '6 Java Developers (800$-1,500$ net)', 'フルタイム', '１２００円', 'Tokyo or Ikebukuro', '04-23-2018', '05-31-2018', '<p>フルタイム</p>', '<p>フルタイム</p>', '<p>フルタイム</p>', 1, NULL, NULL, '2018-04-23 11:28:06', '2018-04-23 11:28:06');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `active`, `created_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 1, NULL, NULL, '2018-04-18 08:31:03', '2018-04-18 08:31:03'),
(2, 'Manage', 1, NULL, NULL, '2018-04-18 08:31:19', '2018-04-18 08:31:19'),
(3, 'User', 1, NULL, NULL, '2018-04-18 08:31:41', '2018-04-18 08:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `language_id`, `title`, `introduction`, `content_text`, `image_url`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cung cấp lập trình viên', '<p>Cung c&acirc;́p lập trình viên</p>', '<p>Cung c&acirc;́p lập trình viên</p>', 'citynow/images/page/service/courseact.png', 1, 1, NULL, NULL, '2018-04-09 13:37:12', '2018-04-19 15:12:14'),
(2, 1, 'Cung cấp lập trình viên', '<p>Cung cấp lập tr&igrave;nh vi&ecirc;n l&agrave; một trong những lĩnh vực mũi nhọn của Citynow. Đội ngũ lập tr&igrave;nh vi&ecirc;n được đ&agrave;o tạo b&agrave;i bản, c&oacute; kỹ năng&nbsp;l&agrave;m việc độc lập v&agrave; teamwork cao. Ch&uacute;ng t&ocirc;i lu&ocirc;n đạt được sự h&agrave;i l&ograve;ng v&agrave; đ&aacute;nh gi&aacute; cao về chất lượng c&ocirc;ng việc.</p>\r\n\r\n<p>Citynow hiện đang l&agrave; đơn vị cung cấp lập tr&igrave;nh vi&ecirc;n l&agrave;m việc onsite hoặc offshore cho c&aacute;c c&ocirc;ng ty phần mềm khu vực Ch&acirc;u &Aacute;, đặc biệt l&agrave; Nhật Bản ở c&aacute;c vị tr&iacute;: Java, PHP, Front- end, Blockchain, Ruby, Forex,SCALA, Android, IOS&hellip; Với sự l&agrave;m việc chuy&ecirc;n nghiệp, chịu được &aacute;p lực cao trong mọi ho&agrave;n cảnh c&ocirc;ng việc v&agrave; kh&ocirc;ng ngừng cập nhật, đổi mới đ&atilde; tạo ra sản phẩm ho&agrave;n thiện nhất.</p>\r\n\r\n<p>Đội ngũ lập tr&igrave;nh vi&ecirc;n người Việt c&oacute; lợi thế trong việc sử dụng Tiếng Anh v&agrave; lập tr&igrave;nh vi&ecirc;n người Nhật, đ&atilde; tạo n&ecirc;n sự đa văn h&oacute;a trong c&ocirc;ng ty. Từ đ&oacute;, l&agrave;m n&ecirc;n thương hiệu &ldquo;<strong>Con người Việt, phong c&aacute;ch Nhật</strong>&rdquo; sẵn s&agrave;ng cải tiến v&agrave; bứt ph&aacute; trong c&ocirc;ng nghệ.</p>', '<p>Cung cấp lập tr&igrave;nh vi&ecirc;n l&agrave; một trong những lĩnh vực mũi nhọn của Citynow. Đội ngũ lập tr&igrave;nh vi&ecirc;n được đ&agrave;o tạo b&agrave;i bản, c&oacute; kỹ năng&nbsp;l&agrave;m việc độc lập v&agrave; teamwork cao. Ch&uacute;ng t&ocirc;i lu&ocirc;n đạt được sự h&agrave;i l&ograve;ng v&agrave; đ&aacute;nh gi&aacute; cao về chất lượng c&ocirc;ng việc.</p>\r\n\r\n<p>Citynow hiện đang l&agrave; đơn vị cung cấp lập tr&igrave;nh vi&ecirc;n l&agrave;m việc onsite hoặc offshore cho c&aacute;c c&ocirc;ng ty phần mềm khu vực Ch&acirc;u &Aacute;, đặc biệt l&agrave; Nhật Bản ở c&aacute;c vị tr&iacute;: Java, PHP, Front- end, Blockchain, Ruby, Forex,SCALA, Android, IOS&hellip; Với sự l&agrave;m việc chuy&ecirc;n nghiệp, chịu được &aacute;p lực cao trong mọi ho&agrave;n cảnh c&ocirc;ng việc v&agrave; kh&ocirc;ng ngừng cập nhật, đổi mới đ&atilde; tạo ra sản phẩm ho&agrave;n thiện nhất.</p>\r\n\r\n<p>Đội ngũ lập tr&igrave;nh vi&ecirc;n người Việt c&oacute; lợi thế trong việc sử dụng Tiếng Anh v&agrave; lập tr&igrave;nh vi&ecirc;n người Nhật, đ&atilde; tạo n&ecirc;n sự đa văn h&oacute;a trong c&ocirc;ng ty. Từ đ&oacute;, l&agrave;m n&ecirc;n thương hiệu &ldquo;<strong>Con người Việt, phong c&aacute;ch Nhật</strong>&rdquo; sẵn s&agrave;ng cải tiến v&agrave; bứt ph&aacute; trong c&ocirc;ng nghệ.</p>', 'citynow/images/page/service/cung cấp lập trình viên.jpg', 2, 1, NULL, NULL, '2018-04-19 15:13:49', '2018-04-19 15:13:49'),
(3, 1, 'a', '<p>a</p>', 'content_text', 'citynow/images/page/service/sangtao2.jpg', 1, 1, NULL, NULL, '2018-04-27 19:50:28', '2018-04-27 19:50:28'),
(4, 1, 'a', '<p>a</p>', '<p>a</p>', 'citynow/images/page/service/sangtao2.jpg', 1, 1, NULL, NULL, '2018-04-27 19:51:24', '2018-04-27 19:51:24');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `language_id`, `name`, `image_url`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(2, 1, '002', 'citynow/images/page/slides/banner1.png', 2, 1, NULL, NULL, '2018-04-09 12:17:48', '2018-04-27 14:54:44'),
(3, 1, '003', 'citynow/images/page/slides/banner006.png', 3, 1, NULL, NULL, '2018-04-09 12:18:00', '2018-04-27 14:55:15'),
(4, 1, '001', 'citynow/images/page/slides/banner005.png', 1, 1, NULL, NULL, '2018-04-09 12:18:33', '2018-04-27 14:55:03'),
(6, 2, '003', 'citynow/images/page/slides/003.jpg', 3, 1, NULL, NULL, '2018-04-09 12:19:00', '2018-04-09 12:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `teamtestimonials`
--

CREATE TABLE `teamtestimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teamtestimonials`
--

INSERT INTO `teamtestimonials` (`id`, `language_id`, `name`, `position`, `location`, `description`, `image_url`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(3, 1, 'Hương Ami', 'Leader', 'HCM, Việt Nam', '<p>Hương Ami</p>', 'citynow/images/page/teamtestimonial/huong.jpg', 1, 1, NULL, NULL, '2018-04-11 14:40:29', '2018-04-11 14:40:29'),
(4, 2, 'Hương Ami', 'Leader', 'Tokyo or Ikebukuro', '<p>Hương Ami</p>', 'citynow/images/page/teamtestimonial/huong.jpg', 1, 1, NULL, NULL, '2018-04-11 14:41:10', '2018-04-11 14:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `phone`, `address`, `remember_token`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Nguyễn Thanh Bình', 'thanhbinh.nguyen@citynow.vn', '$2y$10$Ap3Jag7vj2aed7LFDqvEWeWoV4blXX8lgQ8geFWX.SkQgtCnAYgyS', '0123456789', '123', 'Glevq0sd3h1P4Znp31lWGTurpDpLKMIMg6YBSiopuxclXjDTzrf2DhYxNEGD', 1, NULL, NULL, '2018-04-18 08:32:20', '2018-04-18 08:32:20'),
(2, 1, 'Nguyễn Thanh Bình', 'thanhbinh.nguyen@citynow.vn', '$2y$10$LaD0ELt3NHivljMhQlhH1ezBbHMhfmh1OVkUYOkvfU1T5OXRYHQR.', '0123456789', 'a', NULL, 1, NULL, NULL, '2018-04-18 14:54:43', '2018-04-18 14:54:43'),
(4, 1, 'Admin', 'admin@gmail.com', '$2y$10$Pz4oxKA/1KnX/HSoyVSwhuRO5s1sv5qtvqEv4meuvgU11lHAKaiNq', '0123456789', 'a', '7pN1SVx3HRJe1Ju2pLr020dP1vW8iKwpFxx9OXYr9u7960cJwKN1BdtHttjB', 1, NULL, NULL, '2018-04-24 08:37:58', '2018-04-24 08:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `welfares`
--

CREATE TABLE `welfares` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `welfares`
--

INSERT INTO `welfares` (`id`, `language_id`, `name`, `description`, `image_url`, `order_number`, `active`, `create_user_id`, `updated_user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 'Chính sách công tác phí', '<p>C&ocirc;ng ty quy định mức C&ocirc;ng t&aacute;c ph&iacute; cụ thể cho từng cấp Nh&acirc;n vi&ecirc;n, đảm bảo t&iacute;nh đồng bộ v&agrave; c&ocirc;ng bằng với mục đ&iacute;ch:</p>\r\n\r\n<ul>\r\n	<li>Đ&aacute;p ứng đầy đủ quyền lợi cho Nh&acirc;n vi&ecirc;n khi đi c&ocirc;ng t&aacute;c.</li>\r\n	<li>Cung cấp c&aacute;c nhu cầu thiết yếu, hỗ trợ Nh&acirc;n vi&ecirc;n ho&agrave;n th&agrave;nh tốt c&ocirc;ng việc được giao trong chuyến đi c&ocirc;ng t&aacute;c.</li>\r\n	<li>Tạo cho C&ocirc;ng nh&acirc;n vi&ecirc;n cảm gi&aacute;c thoải m&aacute;i, được quan t&acirc;m, được hỗ trợ từ Ban Gi&aacute;m đốc v&agrave; c&aacute;c ph&ograve;ng ban chức năng.</li>\r\n</ul>', 'citynow/images/page/welfare/people.png', 2, 1, NULL, NULL, '2018-04-10 16:03:19', '2018-04-11 15:05:07'),
(3, 1, 'Chính sách khen thưởng', '<p>H&agrave;ng th&aacute;ng, căn cứ v&agrave;o kết quả l&agrave;m việc của CBCNV, C&ocirc;ng ty c&oacute; ch&iacute;nh s&aacute;ch khen thưởng đối với c&aacute;c tập thể hoặc c&aacute; nh&acirc;n c&oacute; th&agrave;nh t&iacute;ch hoặc s&aacute;ng kiến trong hoạt động sản xuất kinh doanh, phần thưởng được t&iacute;nh v&agrave; trả ngay v&agrave;o lương th&aacute;ng đ&oacute; của CBCNV.<br />\r\nNgo&agrave;i ra, v&agrave;o cuối mỗi năm t&agrave;i ch&iacute;nh, căn cứ v&agrave;o kết quả hoạt động kinh doanh, C&ocirc;ng ty sẽ c&oacute; ch&iacute;nh s&aacute;ch khen thưởng th&agrave;nh t&iacute;ch dựa tr&ecirc;n mức độ ho&agrave;n th&agrave;nh chỉ ti&ecirc;u c&aacute; nh&acirc;n trong đ&aacute;nh gi&aacute; định kỳ h&agrave;ng th&aacute;ng. Mức thưởng n&agrave;y sẽ được quy định cụ thể, chi tiết v&agrave; c&ocirc;ng bằng cho từng cấp độ C&ocirc;ng nh&acirc;n vi&ecirc;n. Mục đ&iacute;ch:</p>\r\n\r\n<ul>\r\n	<li>Khen thưởng C&ocirc;ng nh&acirc;n vi&ecirc;n theo mức độ ho&agrave;n th&agrave;nh mục ti&ecirc;u c&aacute; nh&acirc;n, tạo động lực l&agrave;m việc.</li>\r\n	<li>Tạo một m&ocirc;i trường thi đua l&agrave;nh mạnh để C&ocirc;ng nh&acirc;n vi&ecirc;n ph&aacute;t huy hết tiềm năng bản th&acirc;n, th&uacute;c đẩy C&ocirc;ng ty ph&aacute;t triển vững mạnh.</li>\r\n	<li>Đ&aacute;p ứng nhu cầu được c&ocirc;ng nhận của C&ocirc;ng nh&acirc;n vi&ecirc;n, l&agrave; phương tiện để th&ocirc;ng b&aacute;o rộng r&atilde;i th&agrave;nh t&iacute;ch v&agrave; sự nỗ lực c&aacute; nh&acirc;n m&agrave; C&ocirc;ng nh&acirc;n vi&ecirc;n đ&atilde; thể hiện.</li>\r\n</ul>', NULL, 3, 1, NULL, NULL, '2018-04-10 16:03:43', '2018-04-10 16:03:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicants_language_id_foreign` (`language_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clients_language_id_foreign` (`language_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customers_language_id_foreign` (`language_id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `footer_language_id_foreign` (`language_id`);

--
-- Indexes for table `introductions`
--
ALTER TABLE `introductions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `introductions_language_id_foreign` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logos_language_id_foreign` (`language_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_language_id_foreign` (`language_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_language_id_foreign` (`language_id`);

--
-- Indexes for table `recuitments`
--
ALTER TABLE `recuitments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recuitments_language_id_foreign` (`language_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_language_id_foreign` (`language_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slides_language_id_foreign` (`language_id`);

--
-- Indexes for table `teamtestimonials`
--
ALTER TABLE `teamtestimonials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teamtestimonials_language_id_foreign` (`language_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `welfares`
--
ALTER TABLE `welfares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `welfares_language_id_foreign` (`language_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `introductions`
--
ALTER TABLE `introductions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `recuitments`
--
ALTER TABLE `recuitments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teamtestimonials`
--
ALTER TABLE `teamtestimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `welfares`
--
ALTER TABLE `welfares`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `applicants`
--
ALTER TABLE `applicants`
  ADD CONSTRAINT `applicants_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `footer`
--
ALTER TABLE `footer`
  ADD CONSTRAINT `footer_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `introductions`
--
ALTER TABLE `introductions`
  ADD CONSTRAINT `introductions_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `logos`
--
ALTER TABLE `logos`
  ADD CONSTRAINT `logos_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `recuitments`
--
ALTER TABLE `recuitments`
  ADD CONSTRAINT `recuitments_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `slides`
--
ALTER TABLE `slides`
  ADD CONSTRAINT `slides_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `teamtestimonials`
--
ALTER TABLE `teamtestimonials`
  ADD CONSTRAINT `teamtestimonials_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `welfares`
--
ALTER TABLE `welfares`
  ADD CONSTRAINT `welfares_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
