<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
