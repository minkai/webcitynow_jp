<?php

namespace App\Http\Controllers;
use App\Introduction;
use App\Menu;
use App\Language;
use App\News;
use App\Recuitment;
use App\Slide;
use App\Customer;
use App\Footer;
use App\Service;
use App\TeamTestimonial;
use App\User;
use App\Welfare;
use App\Applicant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Authenticatable, CanResetPassword;
use Illuminate\Support\Facades\Validator;

class CityNowController extends Controller
{

    public function getIndex()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $languages = Language::all();
        $introductions = Introduction::where('page', "home")->where('language_id', $language_id)->first();
        $services = Service::where('language_id', $language_id)->limit(3)->get();
        $news = News::where('language_id', $language_id)->orderBy('created_at', 'DESC')->paginate(3);
        $customers = Customer::where('language_id', $language_id)->orderBy('order_number')->limit(4)->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $slides = Slide::where('language_id', $language_id)->orderBy('order_number')->get();
        return view('citynow.home', compact('menus', 'languages', 'introductions', 'services', 'news', 'slides', 'customers', 'footer', 'language_text', 'language_id'));
    }
    // citynowSA
    public function SAindex()
    {
        $language_id = 1;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $header = Introduction::where('page', "citynowsa_header")->where('language_id', $language_id)->first();
        return view('citynow.citynowSA',compact('language_id', 'header', 'menus', 'footer'));
    }
    // end citynowSA
    public function getAbout()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $header = Introduction::where('page', "about_header")->where('language_id', $language_id)->first();
        $company_informations = Introduction::where('page', "about_company_infomations")->where('language_id', $language_id)->orderBy('order_number')->first();
        $company_imgs = Introduction::where('page', "company_img")->where('language_id', $language_id)->orderBy('order_number')->get();
        // var_dump($company_imgs); die();
        $abouts = Introduction::where('page', "about")->where('language_id', $language_id)->orderBy('order_number')->get();
        $values = Introduction::where('page', "value")->where('language_id', $language_id)->orderBy('order_number')->limit(3)->get();
        return view('citynow.about', compact('menus', 'languages', 'header', 'company_informations', 'company_imgs', 'abouts', 'footer', 'language_text', 'language_id', 'values'));
    }
    public function getService(Request $req)
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $customers = Customer::where('language_id', $language_id)->orderBy('order_number')->get();
        $services = Service::where('language_id', $language_id)->orderBy('order_number')->get();
        $header = Introduction::where('page', "service_header")->where('language_id', $language_id)->first();
        return view('citynow.services', compact('menus', 'languages', 'customers', 'footer', 'language_text', 'language_id', 'services', 'header'));
    }
    public function getServiceDetail($id)
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $service = Service::where('id', $id)->first();
        $header = Introduction::where('page', "service_header")->where('language_id', $language_id)->first();
        return view('citynow.service_detail', compact('menus', 'languages', 'footer', 'language_text', 'language_id', 'service', 'header'));
    }
    public function getStories()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $news = News::where('language_id', $language_id)->orderBy('created_at', 'DESC')->paginate(5);
        $header = Introduction::where('page', "news_header")->where('language_id', $language_id)->first();
        return view('citynow.stories', compact('menus', 'languages', 'footer', 'news', 'language_text', 'language_id', 'header'));
    }
    public function getStoryDetail($id)
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        return view('citynow.story_detail', compact('menus', 'languages', 'footer', 'language_text', 'language_id'));
    }
    public function getNews()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $news = News::where('language_id', $language_id)->orderBy('created_at', 'DESC')->paginate(5);
        $header = Introduction::where('page', "news_header")->where('language_id', $language_id)->first();
        return view('citynow.stories', compact('menus', 'languages', 'footer', 'news', 'language_text', 'language_id', 'header'));
    }
    public function getNewsDetail($id)
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $news = News::where('id', $id)->first();
        $latest_news = News::where('language_id', $language_id)->where('id', '<>', $id)->orderBy('created_at', 'DESC')->paginate(3);
        $header = Introduction::where('page', "news_header")->where('language_id', $language_id)->first();
        return view('citynow.news_details', compact('menus', 'languages', 'latest_news', 'footer', 'news', 'language_text', 'language_id', 'header'));
    }
    public function getCareer2()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $recuitments = Recuitment::where('language_id', $language_id)->orderBy('order_number')->get();
        return view('citynow.career', compact('menus', 'languages', 'recuitments', 'footer', 'language_text', 'language_id'));
    }
    public function getCareer()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $welfares = Welfare::where('language_id', $language_id)->orderBy('order_number')->get();
        $teamTestimonials = TeamTestimonial::where('language_id', $language_id)->orderBy('order_number')->get();
        $recuitments = Recuitment::where("end_date", ">=", date('m-d-Y'))->where('language_id', $language_id)->get();
        $header = Introduction::where('page', "career_header")->where('language_id', $language_id)->first();
        return view('citynow.career', compact('menus', 'languages', 'recuitments', 'footer', 'language_text', 'language_id', 'welfares', 'teamTestimonials', 'header'));
    }
    public function getJob($id)
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $header = Introduction::where('page', "career_header")->where('language_id', $language_id)->first();
        $recuitment = Recuitment::find($id);
        return view('citynow.job', compact('menus', 'languages', 'recuitment', 'footer', 'language_text', 'language_id', 'header'));
    }
    public function postApplyOnline(Request $req)
    {
        $this->validate($req,
            [
                'file_cv'=>'required|mimes:doc,docx,pdf|max:2048',
                'email'=>'required',
                'name'=>'required',
            ]
        );
        $v = Validator::make($req->all(), [
            'file_cv'=>'required|mimes:doc,docx,pdf|max:2048',
            'email'=>'required',
            'name'=>'required',
        ]);
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        if($req->hasFile('file_cv')) {
            $file = $req->file('file_cv');
            $fileName = 'CV_'.$req->name . '.' . $file->getClientOriginalExtension();
            $attachmentUrl = 'citynow/CV/applicants/'.$fileName;
            $file->move('citynow/CV/applicants/',$fileName);
            $applicant = new Applicant();
            $applicant->language_id = $language_id;
            $applicant->name = $req->name;
            $applicant->email = $req->email;
            $applicant->introduction = $req->input('introduction');
            $applicant->attachment_url = $attachmentUrl;
            $applicant->active = true;
            $applicant->save();
            return redirect()->back()->with('success_message','Ứng tuyển online thành công');
        }
        return redirect()->back()->withErrors($v->errors());
    }
    public function getCustomers()
    {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }
        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $customers = Customer::where('language_id', $language_id)->orderBy('order_number')->get();
        $header = Introduction::where('page', "customer_header")->where('language_id', $language_id)->first();
        return view('citynow.customer', compact('menus', 'languages', 'footer', 'customers', 'language_text', 'language_id', 'header'));
    }
    public function getContact() {
        $language_id = 1;
        $domain = \Request::root();
        if($domain == "http://citynow.vn") {
            $language_id = 1;
        } else {
            if($domain == "http://citynow.jp") {
                $language_id = 2;
            } else {
                $url = \Request::path();
                $arrLang = str_split($url, 2);
                if($arrLang[0] == "jp") {
                    $language_id = 2;
                }
            }
        }

        $language_text = Language::where('id', $language_id)->first()->name;
        $menus = Menu::where('language_id', $language_id)->orderBy('order_number', 'DESC')->get();
        $footer = Footer::where('language_id', $language_id)->first();
        $languages = Language::all();
        $header = Introduction::where('page', "contact_header")->where('language_id', $language_id)->first();
        return view('citynow.contact', compact('menus', 'languages', 'footer', 'language_text', 'language_id', 'header'));
    }
}