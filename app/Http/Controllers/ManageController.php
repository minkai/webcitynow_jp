<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Language;
use App\Slide;
use App\User;
use App\Role;
use App\News;
use App\Customer;
use App\Introduction;
use App\Recuitment;
use App\Footer;
use App\Service;
use App\Welfare;
use App\TeamTestimonial;
use App\Applicant;
use App;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ManageController extends Controller
{
    public function getLogin()
    {
        if(Auth::check()) {
            return redirect('language');
        }
        return view('auth.login');
    }
    public function postLogin(Request $req)
    {
        $v = Validator::make($req->all(), [
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);
        $data=[
            'email'=>$req->email,
            'password'=>$req->password
        ];
        if(Auth::attempt($data)){
            return redirect('language');
        }else{
            return redirect()->back()->withErrors($v->errors());
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('manage/login');
    }

    public function getChangePassword()
    {
        if(Auth::check()) {
            return view('auth.passwords.change');
        }
        return view('auth.login');
    }
    public function postChangePassword(Request $req)
    {
        if (!(Hash::check($req->get('current-password'), Auth::user()->password))) {
        // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($req->get('current-password'), $req->get('new-password')) == 0){
        //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
//        $validatedData = $req->validate([
//            'current-password' => 'required',
//            'new-password' => 'required|min:6',
//        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($req->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }

    //    Begin Language
    public function getLanguages()
    {
        $languages = Language::paginate(10);
        return view('manage.language.list', compact('languages'));
    }
    public function getAddLanguage()
    {
        return view('manage.language.add');
    }
    public function postAddLanguage(Request $req)
    {
        $language = new Language();
        $language->name = $req->name;
        $language->alias = $req->alias;
        $language->active = true;
        $language->save();
        return redirect('language/add')->with('success_message','Add New Language successful');
    }
    public function getEditLanguage($id)
    {
        $language = Language::find($id);
        return view('manage.language.edit',compact('language'));
    }
    public function postEditLanguage(Request $req)
    {
        $language = Language::where('id',$req->id)->first();
        $language->name = $req->name;
        $language->alias = $req->alias;
        $language->save();
        return redirect('language')->with('success_message','Update Language successful');
    }
    public function getDeleteLanguage($id)
    {
        Language::find($id)->delete();
        return redirect('language')->with('success_message','Delete Language successful');
    }
//    End language

//    Begin Menu
    public function getMenu()
    {
        $menus = Menu::paginate(10);
        return view('manage.menu.list', compact('menus'));
    }
    public function getAddMenu()
    {
        $languages = Language::all();
        return view('manage.menu.add', compact('languages'));
    }
    public function postAddMenu(Request $req)
    {
        $menu = new Menu();
        $menu->name = $req->name;
        $menu->alias = $req->alias;
        $menu->order_number = $req->order_number;
        $menu->language_id = $req->language_id;
        $menu->active = true;
//        $menu->create_user_id = 1;
        $menu->save();
        return redirect('menu/add')->with('success_message','Add New Menu successful');
    }
    public function getEditMenu($id)
    {
        $languages = Language::all();
        $menu = Menu::find($id);
        return view('manage.menu.edit',compact('menu', 'languages'));
    }
    public function postEditMenu(Request $req)
    {
        $menu = Menu::where('id',$req->id)->first();
        $menu->name = $req->name;
        $menu->alias = $req->alias;
        $menu->order_number = $req->order_number;
        $menu->language_id = $req->language_id;
//        $menu->updated__id = 1;
        $menu->save();
        return redirect('menu')->with('success_message','Update Menu successful');
    }
    public function getDeleteMenu($id)
    {
        Menu::find($id)->delete();
        return redirect('menu')->with('success_message','Delete Menu successful');
    }
//    End Menu

//    Begin Slides
    public function getSlides()
    {
        $slides = Slide::paginate(10);
        return view('manage.slide.list', compact('slides'));
    }
    public function getAddSlide()
    {
        $languages = Language::all();
        return view('manage.slide.add', compact('languages'));
    }
    public function postAddSlide(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/slides/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/slides/',$file->getClientOriginalName());
            $slide = new Slide();
            $slide->name = $req->name;
            $slide->image_url = $imageUrl;
            $slide->order_number = $req->order_number;
            $slide->language_id = $req->language_id;
            $slide->active = true;
//        $slide->created_user_id = 1;
            $slide->save();
            return redirect('slide/add')->with('success_message','Add New Slide successful');
        }
        return redirect('slide/add')->with('success_fail','Add New Slide fail');
    }
    public function getEditSlide($id)
    {
        $languages = Language::all();
        $slide = Slide::find($id);
        return view('manage.slide.edit',compact('slide', 'languages'));
    }
    public function postEditSlide(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $slide = Slide::where('id',$req->id)->first();
        $slide->name = $req->name;
        if($req->hasFile('file_image')) {
            \File::delete($slide->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/slides/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/slides/', $file->getClientOriginalName());
            $slide->image_url = $imageUrl;
        }
        $slide->language_id = $req->language_id;
        $slide->order_number = $req->order_number;
//        $slide->updated_user_id = 1;
        $slide->save();
        return redirect('slide')->with('success_message','Update Slide successful');
    }
    public function getDeleteSlide($id)
    {
        Slide::find($id)->delete();
        return redirect('slide')->with('success_message','Delete Slide successful');
    }
//    End slides

//    Begin User
    public function getUsers()
    {
        $users = User::paginate(10);
        return view('manage.user.list', compact('users'));
    }
    public function getAddUser()
    {
        $roles = Role::all();
        return view('manage.user.add', compact('roles'));
    }
    public function postAddUser(Request $req)
    {
        $user = new User();
        $user->role_id = $req->role_id;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->phone = $req->phone_number;
        $user->address = $req->address;
        $user->active = true;
//        $user->created_user_id = 1;
        $user->save();
        return redirect('manage-user/add')->with('success_message','Add New User successful');
    }
    public function getEditUser($id)
    {
        $user = User::where('id',$id)->first();
        $roles = Role::all();
        return view('manage.user.edit',compact('user', 'roles'));
    }
    public function postEditUser(Request $req)
    {
        $user = User::where('id',$req->id)->first();
        $user->role_id = $req->role_id;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->phone = $req->phone_number;
        $user->address = $req->address;
//        $user->updated_user_id = 1;
        $user->save();
        return redirect('manage-user')->with('success_message','Update User successful');
    }
    public function getDeleteUser($id)
    {
        User::find($id)->delete();
        return redirect('manage-user')->with('success_message','Delete User successful');
    }
//    End User

//    Begin Role
    public function getRoles()
    {
        $roles = Role::paginate(10);
        return view('manage.role.list', compact('roles'));
    }
    public function getAddRole()
    {
        return view('manage.role.add');
    }
    public function postAddRole(Request $req)
    {
        $role = new Role();
        $role->name = $req->name;
        $role->active = true;
//        $role->created_user_id = 1;
        $role->save();
        return redirect('manage-role/add')->with('success_message','Add New Role successful');
    }
    public function getEditRole($id)
    {
        $role = Role::find($id)->first();
        return view('manage.role.edit',compact('role'));
    }
    public function postEditRole(Request $req)
    {
        $role = Role::where('id',$req->id)->first();
        $role->name = $req->name;
//        $role->updated_user_id = 1;
        $role->save();
        return redirect('manage-role')->with('success_message','Update Role successful');
    }
    public function getDeleteRole($id)
    {
        Role::find($id)->delete();
        return redirect('manage-role')->with('success_message','Delete Role successful');
    }
//    End Role

//    Begin Introduction
    public function getIntroductions()
    {
        $introductions = Introduction::paginate(10);
        return view('manage.introduction.list', compact('introductions'));
    }
    public function getAddIntroduction()
    {
        $languages = Language::all();
        return view('manage.introduction.add',compact('languages'));
    }
    public function postAddIntroduction(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/introduction/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/introduction/', $file->getClientOriginalName());
            $introduction = new Introduction();
            $introduction->language_id = $req->language_id;
            $introduction->page = $req->page;
            $introduction->title = $req->title;
            $introduction->content_text = $req->input('content_text');
            $introduction->image_url = $imageUrl;
            $introduction->order_number = $req->order_number;
            $introduction->active = true;
//        $role->created_user_id = 1;
            $introduction->save();
            return redirect('introduction/add')->with('success_message', 'Add New Introduction successful');
        }
        return redirect('introduction/add')->with('success_fail', 'Add New Introduction fail');
    }
    public function getEditIntroduction($id)
    {
        $languages = Language::all();
        $introduction = Introduction::find($id);
        return view('manage.introduction.edit',compact('introduction', 'languages'));
    }
    public function postEditIntroduction(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $introduction = Introduction::where('id',$req->id)->first();
        $introduction->language_id = $req->language_id;
        $introduction->title = $req->title;
        $introduction->page = $req->page;
        $introduction->content_text = $req->input('content_text');
        $introduction->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            \File::delete($introduction->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/introduction/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/introduction/', $file->getClientOriginalName());
            $introduction->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $introduction->save();
        return redirect('introduction')->with('success_message','Update Introduction successful');
    }
    public function getDeleteIntroduction($id)
    {
        Introduction::find($id)->delete();
        return redirect('introduction')->with('success_message','Delete Introduction successful');
    }
//    End Introduction

//    Begin News
    public function getNews()
    {
        $news = News::paginate(10);
        return view('manage.news.list', compact('news'));
    }
    public function getAddNews()
    {
        $languages = Language::all();
        return view('manage.news.add',compact('languages'));
    }
    public function postAddNews(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/news/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/news/', $file->getClientOriginalName());
            $news = new News();
            $news->language_id = $req->language_id;
            $news->title = $req->title;
            $news->content = $req->input('content_text');
            $news->image_url = $imageUrl;
            $news->active = true;
//        $role->created_user_id = 1;
            $news->save();
            return redirect('manage-news/add')->with('success_message', 'Add New News successful');
        }
        return redirect('manage-news/add')->with('success_fail', 'Add New News fail');
    }
    public function getEditNews($id)
    {
        $languages = Language::all();
        $news = News::find($id);
        return view('manage.news.edit',compact('news', 'languages'));
    }
    public function postEditNews(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $news = News::where('id',$req->id)->first();
        $news->language_id = $req->language_id;
        $news->title = $req->title;
        $news->content = $req->input('content_text');
        if($req->hasFile('file_image')) {
            \File::delete($news->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/news/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/news/', $file->getClientOriginalName());
            $news->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $news->save();
        return redirect('manage-news')->with('success_message','Update News successful');
    }
    public function getDeleteNews($id)
    {
        News::find($id)->delete();
        return redirect('manage-news')->with('success_message','Delete News successful');
    }
//    End News

//    Begin News
    public function getCustomers()
    {
        $customers = Customer::paginate(10);
        return view('manage.customer.list', compact('customers'));
    }
    public function getAddCustomer()
    {
        $languages = Language::all();
        return view('manage.customer.add',compact('languages'));
    }
    public function postAddCustomer(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/customer/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/customer/', $file->getClientOriginalName());
            $customer = new Customer();
            $customer->language_id = $req->language_id;
            $customer->name = $req->name;
            $customer->link = $req->link;
            $customer->order_number = $req->order_number;
            $customer->introduction = $req->input('introduction');
            $customer->image_url = $imageUrl;
            $customer->active = true;
            $customer->save();
            return redirect('manage-customer/add')->with('success_message', 'Add New Customer successful');
        }
        return redirect('manage-customer/add')->with('success_fail', 'Add New Customer fail');
    }
    public function getEditCustomer($id)
    {
        $languages = Language::all();
        $customer = Customer::find($id);
        return view('manage.customer.edit',compact('customer', 'languages'));
    }
    public function postEditCustomer(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $customer = Customer::where('id',$req->id)->first();
        $customer->language_id = $req->language_id;
        $customer->name = $req->name;
        $customer->link = $req->link;
        $customer->order_number = $req->order_number;
        $customer->introduction = $req->input('introduction');
        if($req->hasFile('file_image')) {
            \File::delete($customer->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/customers/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/customers/', $file->getClientOriginalName());
            $customer->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $customer->save();
        return redirect('manage-customer')->with('success_message','Update Customer successful');
    }
    public function getDeleteCustomer($id)
    {
        Customer::find($id)->delete();
        return redirect('manage-customer')->with('success_message','Delete Customer successful');
    }
//    End News

//    Begin Recuitment
    public function getRecuitments()
    {
        $recuitments = Recuitment::paginate(10);
        return view('manage.recuitment.list', compact('recuitments'));
    }
    public function getAddRecuitment()
    {
        $languages = Language::all();
        return view('manage.recuitment.add',compact('languages'));
    }
    public function postAddRecuitment(Request $req)
    {
        $recuitment = new Recuitment();
        $recuitment->language_id = $req->language_id;
        $recuitment->title = $req->title;
        $recuitment->type = $req->type;
        $recuitment->salary = $req->salary;
        $recuitment->location = $req->location;
        $recuitment->start_date = $req->start_date;
        $recuitment->end_date = $req->end_date;
        $recuitment->job_description = $req->input('job_description');
        $recuitment->job_requirement = $req->input('job_requirement');
        $recuitment->job_benefit = $req->input('job_benefit');
        $recuitment->active = true;

//        $role->created_user_id = 1;
        $recuitment->save();
        return redirect('recuitment/add')->with('success_message','Add New Recuitment successful');
    }
    public function getEditRecuitment($id)
    {
        $languages = Language::all();
        $recuitment = Recuitment::find($id);
        return view('manage.recuitment.edit',compact('recuitment', 'languages'));
    }
    public function postEditRecuitment(Request $req)
    {
        $recuitment = Recuitment::where('id',$req->id)->first();
        $recuitment->language_id = $req->language_id;
        $recuitment->title = $req->title;
        $recuitment->type = $req->type;
        $recuitment->salary = $req->salary;
        $recuitment->location = $req->location;
        $recuitment->start_date = $req->start_date;
        $recuitment->end_date = $req->end_date;
        $recuitment->job_description = $req->input('job_description');
        $recuitment->job_requirement = $req->input('job_requirement');
        $recuitment->job_benefit = $req->input('job_benefit');
//        $role->updated_user_id = 1;
        $recuitment->save();
        return redirect('recuitment')->with('success_message','Update Recuitment successful');
    }
    public function getDeleteRecuitment($id)
    {
        Recuitment::find($id)->delete();
        return redirect('recuitment')->with('success_message','Delete Recuitment successful');
    }
//    End Recuitment

//    Begin Footer
    public function getFooter()
    {
        $footers = Footer::paginate(10);
        return view('manage.footer.list', compact('footers'));
    }
    public function getAddFooter()
    {
        $languages = Language::all();
        return view('manage.footer.add',compact('languages'));
    }
    public function postAddFooter(Request $req)
    {
        $footer = new Footer();
        $footer->language_id = $req->language_id;
//        $footer->introduction = $req->introduction;
        $footer->link_instagram = $req->link_instagram;
        $footer->link_twitter = $req->link_twitter;
        $footer->link_facebook = $req->link_facebook;
        $footer->title_vn = $req->title_vn;
        $footer->address_vn = $req->address_vn;
        $footer->phone_vn = $req->phone_vn;
        $footer->email_vn = $req->email_vn;
        $footer->title_jp = $req->title_jp;
        $footer->address_jp = $req->address_jp;
        $footer->phone_jp = $req->phone_jp;
        $footer->email_jp = $req->email_jp;
        $footer->active = true;
//        $role->created_user_id = 1;
        $footer->save();
        return redirect('footer/add')->with('success_message','Add New Footer successful');
    }
    public function getEditFooter($id)
    {
        $languages = Language::all();
        $footer = Footer::find($id);
        return view('manage.footer.edit',compact('footer', 'languages'));
    }
    public function postEditFooter(Request $req)
    {
        $footer = Footer::where('id',$req->id)->first();
        $footer->language_id = $req->language_id;
//        $footer->introduction = $req->introduction;
        $footer->link_instagram = $req->link_instagram;
        $footer->link_twitter = $req->link_twitter;
        $footer->link_facebook = $req->link_facebook;
        $footer->title_vn = $req->title_vn;
        $footer->address_vn = $req->address_vn;
        $footer->phone_vn = $req->phone_vn;
        $footer->email_vn = $req->email_vn;
        $footer->title_jp = $req->title_jp;
        $footer->address_jp = $req->address_jp;
        $footer->phone_jp = $req->phone_jp;
        $footer->email_jp = $req->email_jp;
//        $role->updated_user_id = 1;
        $footer->save();
        return redirect('footer')->with('success_message','Update Footer successful');
    }
    public function getDeleteFooter($id)
    {
        Footer::find($id)->delete();
        return redirect('footer')->with('success_message','Delete Footer successful');
    }
//    End Footer

//    Begin Service
    public function getServices()
    {
        $services = Service::paginate(10);
        return view('manage.service.list', compact('services'));
    }
    public function getAddService()
    {
        $languages = Language::all();
        return view('manage.service.add',compact('languages'));
    }
    public function postAddService(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/service/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/service/', $file->getClientOriginalName());
            $service = new Service();
            $service->language_id = $req->language_id;
            $service->title = $req->title;
            $service->introduction = $req->input('introduction');
            $service->content_text = $req->input('content_text');
            $service->order_number = $req->order_number;
            $service->image_url = $imageUrl;
            $service->active = true;
//        $role->created_user_id = 1;
            $service->save();
            return redirect('manage-service/add')->with('success_message', 'Add New Service successful');
        }
        return redirect('manage-service/add')->with('success_fail', 'Add New Service fail');
    }
    public function getEditService($id)
    {
        $languages = Language::all();
        $service = Service::find($id);
        return view('manage.service.edit',compact('service', 'languages'));
    }
    public function postEditService(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $service = Service::where('id',$req->id)->first();
        $service->language_id = $req->language_id;
        $service->title = $req->title;
        $service->introduction = $req->input('introduction');
        $service->content_text = $req->input('content_text');
        $service->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            \File::delete($service->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/service/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/service/', $file->getClientOriginalName());
            $service->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $service->save();
        return redirect('manage-service')->with('success_message','Update Service successful');
    }
    public function getDeleteService($id)
    {
        Service::find($id)->delete();
        return redirect('manage-service')->with('success_message','Delete Service successful');
    }
//    End Service

//    Begin Welfare
    public function getWelfares()
    {
        $welfares = Welfare::paginate(10);
        return view('manage.welfare.list', compact('welfares'));
    }
    public function getAddWelfare()
    {
        $languages = Language::all();
        return view('manage.welfare.add',compact('languages'));
    }
    public function postAddWelfare(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $welfare = new Welfare();
        $welfare->language_id = $req->language_id;
        $welfare->name = $req->name;
        $welfare->description = $req->input('description');
        $welfare->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/welfare/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/welfare/', $file->getClientOriginalName());
            $welfare->image_url = $imageUrl;
        } else {
            $welfare->image_url = null;
        }
        $welfare->active = true;
//        $role->created_user_id = 1;
        $welfare->save();
        return redirect('manage-welfare/add')->with('success_message', 'Add New Welfare successful');
//        return redirect('manage-welfare/add')->with('success_fail', 'Add New Welfare fail');
    }
    public function getEditWelfare($id)
    {
        $languages = Language::all();
        $welfare = Welfare::find($id);
        return view('manage.welfare.edit',compact('welfare', 'languages'));
    }
    public function postEditWelfare(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $welfare = Welfare::where('id',$req->id)->first();
        $welfare->language_id = $req->language_id;
        $welfare->name = $req->name;
        $welfare->description = $req->input('description');
        $welfare->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            \File::delete($welfare->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/welfare/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/welfare/', $file->getClientOriginalName());
            $welfare->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $welfare->save();
        return redirect('manage-welfare')->with('success_message','Update Welfare successful');
    }
    public function getDeleteWelfare($id)
    {
        Welfare::find($id)->delete();
        return redirect('manage-welfare')->with('success_message','Delete Welfare successful');
    }
//    End Welfare

//    Begin Team Testimonial
    public function getTeamTestimonials()
    {
        $teamtestimonials = TeamTestimonial::paginate(10);
        return view('manage.teamtestimonial.list', compact('teamtestimonials'));
    }
    public function getAddTeamTestimonial()
    {
        $languages = Language::all();
        return view('manage.teamtestimonial.add',compact('languages'));
    }
    public function postAddTeamTestimonial(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $teamTestimonial = new TeamTestimonial();
        $teamTestimonial->language_id = $req->language_id;
        $teamTestimonial->name = $req->name;
        $teamTestimonial->position = $req->position;
        $teamTestimonial->location = $req->location;
        $teamTestimonial->description = $req->input('description');
        $teamTestimonial->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/teamtestimonial/' . $file->getClientOriginalName();
            $file->move('citynow/images/page/teamtestimonial/', $file->getClientOriginalName());
            $teamTestimonial->image_url = $imageUrl;
        }
        $teamTestimonial->active = true;
//        $role->created_user_id = 1;
        $teamTestimonial->save();
        return redirect('manage-team-testimonial/add')->with('success_message', 'Add New Team Testimonial successful');
//        return redirect('manage-team-testimonial/add')->with('success_fail', 'Add New Team Testimonial fail');
    }
    public function getEditTeamTestimonial($id)
    {
        $languages = Language::all();
        $teamtestimonial = TeamTestimonial::find($id);
        return view('manage.teamtestimonial.edit',compact('teamtestimonial', 'languages'));
    }
    public function postEditTeamTestimonial(Request $req)
    {
        $this->validate($req,
            [
                'file_image'=>'image|mimes:jpeg,jpg,png,bmp,gif,svg|max:2048'
            ]
        );
        $teamTestimonial = TeamTestimonial::where('id',$req->id)->first();
        $teamTestimonial->language_id = $req->language_id;
        $teamTestimonial->name = $req->name;
        $teamTestimonial->position = $req->position;
        $teamTestimonial->location = $req->location;
        $teamTestimonial->description = $req->input('description');
        $teamTestimonial->order_number = $req->order_number;
        if($req->hasFile('file_image')) {
            \File::delete($teamTestimonial->image_url);
            $file = $req->file('file_image');
            $imageUrl = 'citynow/images/page/teamtestimonial/'.$file->getClientOriginalName();
            $file->move('citynow/images/page/teamtestimonial/', $file->getClientOriginalName());
            $teamTestimonial->image_url = $imageUrl;
        }
//        $role->updated_user_id = 1;
        $teamTestimonial->save();
        return redirect('manage-team-testimonial')->with('success_message','Update Team Testimonial successful');
    }
    public function getDeleteTeamTestimonial($id)
    {
        TeamTestimonial::find($id)->delete();
        return redirect('manage-team-testimonial')->with('success_message','Delete Team Testimonial successful');
    }
//    End Team Testimonial

//    Begin Team Testimonial
    public function getApplicants()
    {
        $applicants = App\Applicant::paginate(10);
        return view('manage.applicant.list', compact('applicants'));
    }
    public function getApplicantDetails($id)
    {
        $languages = Language::all();
        $applicant = Applicant::find($id);
        return view('manage.applicant.details',compact('applicant', 'languages'));
    }
    public function getDeleteApplicant($id)
    {
        $applicant = Applicant::find($id);
        $filename = public_path().$applicant->attachment_url;
        \File::delete($filename);
        $applicant->delete();
        return redirect('manage-applicant')->with('success_message','Delete Applicant successful');
    }
    public function goToApplicant()
    {
        return redirect('manage-applicant');
    }
//    End Team Testimonial
}