<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $table = "footer";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
