<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "customer";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
