<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $table = "applicants";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}