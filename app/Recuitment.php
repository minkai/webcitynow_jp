<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recuitment extends Model
{
    protected $table = "recuitments";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
