<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Introduction extends Model
{
    protected $table = "introductions";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
