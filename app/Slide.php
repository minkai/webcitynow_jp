<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = "slides";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
