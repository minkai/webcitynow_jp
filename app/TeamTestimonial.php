<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamTestimonial extends Model
{
    protected $table = "teamtestimonials";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
