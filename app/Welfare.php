<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Welfare extends Model
{
    protected $table = "welfares";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
