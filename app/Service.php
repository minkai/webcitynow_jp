<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = "service";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
