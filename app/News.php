<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";
    public function language(){
        return $this->belongsTo('App\Language','language_id','id');
    }
}
